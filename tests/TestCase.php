<?php

namespace Tests;

use App\Permission;
use App\PermissionRole;
use App\Role;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{

    use CreatesApplication, RefreshDatabase;


    public function loginWithSpecificPermission($title=null)
    {
        $role = factory(Role::class)->create();
        if ($title != null){
            $permission = factory(Permission::class)->create(['title_english' => $title]);
            factory(PermissionRole::class)->create(['role_id' => $role->id, 'permission_id' => $permission->id]);
        }
        return $this->actingAs(factory(User::class)->create(['role_id' => $role->id]));
    }

    public function login()
    {
        return $this->actingAs(factory(User::class)->create());
    }
}
