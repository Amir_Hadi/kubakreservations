<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;


    public function test_authenticated_user_can_see_homepage()
    {
        $this->actingAs(factory('App\User')->create());
        $this->get('/home')->assertStatus(200);
    }

    public function test_un_authenticated_user_can_not_see_home_page()
    {
        $this->get('/home')->assertRedirect('/login');
    }
}
