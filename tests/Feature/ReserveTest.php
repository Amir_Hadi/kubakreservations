<?php

namespace Tests\Feature;

use App\AdminConfig;
use App\Reserve;
use App\ScheduleFood;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReserveTest extends TestCase
{
    use RefreshDatabase;

    public function test_authorized_user_can_reserve_nextweek_schedule()
    {
        $this->loginWithSpecificPermission('food-reserve');
        factory(AdminConfig::class)->create(['day' => 5, 'time' => '12:00 AM']);

        $schedule = factory(ScheduleFood::class)->create(['date' => Carbon::now()->addWeek()]);

        $this->post('reserves', [ 'reserves' =>[
                $schedule->id => 1
            ]
        ]);

        $this->assertDatabaseHas('reserves', [
            'schedule_id' => $schedule->id,
            'user_id' => auth()->id()
        ]);

        $this->assertCount(1, Reserve::all());
    }

    public function test_authorized_user_can_not_reserve_this_week_schedules()
    {
        $this->loginWithSpecificPermission('food-reserve');

        $schedule = factory(ScheduleFood::class)->create(['date' => Carbon::now()]);
        factory(AdminConfig::class)->create(['day' => 5, 'time' => '12:00 AM']);
        $this->post('/reserves', ['reserves' => [
                $schedule->id => 1
            ]
        ]);

        $this->assertDatabaseMissing('reserves', [
            'schedule_id' => $schedule->id,
            'user_id' => auth()->id()
        ]);

        $this->assertCount(0, Reserve::all());
    }

    public function test_reserved_before_can_not_reserve_again()
    {
        $this->loginWithSpecificPermission('food-reserve');
        factory(AdminConfig::class)->create(['day' => 5, 'time' => '12:00 Am']);
        $schedule = factory(ScheduleFood::class)->create(['date' => Carbon::now()->addWeek()]);
        $reserve = factory(Reserve::class)->create(['schedule_id' => $schedule->id, 'user_id' => auth()->id()]);

        $this->post('/reserves', ['reserves' => [
                $reserve->schedule_id => 1
            ]
        ]);

        $this->assertCount(1, Reserve::where([
            ['schedule_id' , '=', $reserve->schedule_id],
            ['user_id' , '=', $reserve->user_id]
        ])->get());
    }

    public function test_unchecked_day_will_be_canceled_for_authorized_user()
    {
        $this->loginWithSpecificPermission('food-reserve');

        $schedule = factory(ScheduleFood::class)->create(['date' => Carbon::now()->addWeek()]);
        $anotherschedule = factory(ScheduleFood::class)->create(['date' => Carbon::now()->addWeek()->addDay()]);

        factory(AdminConfig::class)->create(['day' => 5, 'time' => '12:00 AM']);

        factory(Reserve::class)->create(['schedule_id' => $schedule->id, 'user_id' => auth()->id()]);
        factory(Reserve::class)->create(['schedule_id' => $schedule->id, 'user_id' => auth()->id()]);

        $this->post('/reserves', [
            'reserves' => [
                $schedule->id => 1
            ]
        ]);

        $this->assertDatabaseMissing('reserves', [
           'schedule_id' => $anotherschedule->id,
           'user_id' => auth()->id()
        ]);

    }

    public function test_unauthenticated_user_can_not_reserve()
    {
        factory(AdminConfig::class)->create(['day' => 5, 'time' => '12:00 Am']);
        $schedule = factory(ScheduleFood::class)->create(['date' => Carbon::now()->addWeek()]);

        $this->post('/reserves', [
           'reserves' => [
               $schedule->id => 1
           ]
        ])->assertRedirect('/login');

        $this->assertDatabaseMissing('reserves', [
            'schedule_id' => $schedule->id,
            'user_id' => auth()->id()
        ]);

    }


    public function test_unauthorized_user_can_not_reserve()
    {
        $this->loginWithSpecificPermission();
        factory(AdminConfig::class)->create(['day' => 5, 'time' => '12:00 Am']);
        $schedule = factory(ScheduleFood::class)->create(['date' => Carbon::now()->addWeek()]);

        $this->post('/reserves', [
            'reserves' => [
                $schedule->id => 1
            ]
        ])->assertStatus(403);

        $this->assertDatabaseMissing('reserves', [
            'schedule_id' => $schedule->id,
            'user_id' => auth()->id()
        ]);
    }


}
