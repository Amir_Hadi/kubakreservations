<?php

namespace Tests\Feature;

use App\Food;
use App\PermissionRole;
use App\Role;
use App\ScheduleFood;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FoodTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */


    public function test_admin_can_see_foods()
    {
        $this->loginWithSpecificPermission('read-food');

        $food = factory(Food::class)->create();
        $this->get('/foods')
            ->assertSee($food->name);
    }


    public function test_authorized_user_can_create_food()
    {
        $this->loginWithSpecificPermission('add-food');

        $food = factory(Food::class)->make();

        $this->post(route('foods.store'), $food->toArray());

        $this->assertDatabaseHas('foods', [
            'name' => $food->name,
            'description' => $food->description,
            'cost' => $food->cost
        ]);
    }

    public function test_authorized_user_can_edit_food()
    {
        $this->loginWithSpecificPermission('edit-food');

        $food = factory(Food::class)->create();

        $food->name = 'name has changed';

        $this->patch(route('foods.update', $food), $food->toArray());

        $this->assertDatabaseHas('foods', [
            'id' => $food->id,
            'name' => 'name has changed',
            'description' => $food->description,
        ]);

    }

    public function test_food_name_is_required()
    {
        $this->loginWithSpecificPermission('create-food');

        $this->makeFood(['name' => null])
            ->assertSessionHasErrors(['name']);

        $this->assertDatabaseMissing('foods', ['name' => null]);
    }

    public function test_food_description_is_required()
    {
        $this->loginWithSpecificPermission('create-food');

        $this->makeFood(['description' => null])
            ->assertSessionHasErrors(['description']);

        $this->assertDatabaseMissing('foods', [
           'description' => null
        ]);
    }

    public function test_food_cost_is_required()
    {
        $this->loginWithSpecificPermission('create-food');

        $this->makeFood(['cost' => null]);
        $this->assertDatabaseMissing('foods', [
           'cost' => null
        ]);
    }


    public function makeFood($overrides = [])
    {
        $this->actingAs(factory(User::class)->create());

        $food = factory(Food::class)->make($overrides);

        return $this->post(route('foods.store'), $food->toArray());
    }

    public function test_authorized_user_can_delete_a_food_which_not_used_in_schedules()
    {
        $this->loginWithSpecificPermission('delete-food');

        $food = factory(Food::class)->create();

        $this->delete(route('foods.destroy', $food))->assertRedirect(route('foods.index'));

        $this->assertDatabaseMissing('foods', [
           'id' => $food->id,
           'name' => $food->name
        ]);

        $this->assertCount(0, Food::all());
    }

    public function test_a_food_with_a_schedule_can_not_be_deleted()
    {
        $this->loginWithSpecificPermission('delete-food');

        $food = factory(Food::class)->create();

        factory(ScheduleFood::class)->create(['food_id' => $food->id]);

        $this->delete(route('foods.destroy', $food))->assertStatus(403);

        $this->assertDatabaseHas('foods', [
            'id' => $food->id,
            'name' => $food->name
        ]);

        $this->assertCount(1, Food::all());
    }

}
