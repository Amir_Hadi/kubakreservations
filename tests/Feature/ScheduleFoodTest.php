<?php

namespace Tests\Feature;

use App\Food;
use App\ScheduleFood;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScheduleFoodTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_authorized_user_can_create_schedule()
    {
        $this->loginWithSpecificPermission('add-schedule');
        $schedule = factory(ScheduleFood::class)->make();
        $this->post('/schedules/store', ['schedules' => [
                $schedule->date->format('Y-m-d') => $schedule->food_id
            ]
        ]);
        $this->assertDatabaseHas('schedule_foods', [
            'date' => $schedule->date->format('Y-m-d H:i:s'),
            'food_id' => $schedule->food_id
        ]);

    }

    public function test_unauthorized_user_can_not_create_schedules()
    {
        $this->actingAs(factory(User::class)->create());
        $schedule = factory(ScheduleFood::class)->make();
        $this->post('/schedules/store', ['schedules' => [
                $schedule->date->format('Y-m-d') => $schedule->food_id
            ]
        ])->assertStatus(403);
        $this->assertDatabaseMissing('schedule_foods', [
            'date' => $schedule->date->format('Y-m-d H:i:s'),
            'food_id' => $schedule->food_id
        ]);
    }

    public function test_schedule_has_relation_with_foods()
    {
        $schedule = factory(ScheduleFood::class)->create();
        $this->assertInstanceOf('App\Food', $schedule->food );
    }

    public function test_schedule_date_is_unique()
    {
        $this->loginWithSpecificPermission('add-schedule');

        $firstSchedule = factory(ScheduleFood::class)->create(['date' => Carbon::now()->startOfDay()]);

        $food = factory(Food::class)->create();

        $this->post('/schedules/store', ['schedules' =>
            [
                $firstSchedule->date->format('Y-m-d') => $food->id,
            ]
        ]);

        self::assertCount(1, ScheduleFood::all());

        $this->assertDatabaseMissing('schedule_foods', [
            'food_id' => $firstSchedule->food_id
        ]);

        $this->assertCount(1, ['date' => $firstSchedule->date]);
    }

//    public function test_schedule_date_can_not_be_null()
//    {
//
//    }
}
