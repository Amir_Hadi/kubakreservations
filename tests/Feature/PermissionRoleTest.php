<?php

namespace Tests\Feature;

use App\Permission;
use App\PermissionRole;
use App\Role;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PermissionRoleTest extends TestCase
{
    public function test_authorized_user_can_see_specific_roles_permissions()
    {
        $this->loginWithSpecificPermission('read-role');

        $role = factory(Role::class)->create();

        $permission = factory(Permission::class)->create();

        factory(PermissionRole::class)->create([
            'role_id' => $role->id,
            'permission_id' => $permission->id
        ]);

        $this->get('/roles')->assertSee($permission->title_persian);
    }

    public function test_authorized_user_can_create_permission_role()
    {
        $this->loginWithSpecificPermission('add-permissionrole');

        $role = factory(Role::class)->create();

        $firstpermission = factory(Permission::class)->create();
        $secondpermission = factory(Permission::class)->create();

        $this->post("/roles/{$role->id}/permissions/store", [
            $firstpermission->id => 1,
            $secondpermission->id => 1
        ]);

        $this->assertCount(2, Role::all());

        $this->assertCount(3, PermissionRole::all());

        $this->assertDatabaseHas('permission_roles', [
            'permission_id' => $firstpermission->id,
            'role_id' => $role->id
        ]);

        $this->assertDatabaseHas('permission_roles', [
            'permission_id' => $secondpermission->id,
            'role_id' => $role->id
        ]);

    }

    public function test_unchecked_permission_will_revoke_from_permission_role()
    {
        $this->loginWithSpecificPermission('add-permissionrole');

        $role = factory(Role::class)->create();

        $firstpermission = factory(Permission::class)->create();
        $secondpermission = factory(Permission::class)->create();

        $firstpermissionrole = factory(PermissionRole::class)->create([
            'role_id' => $role->id,
            'permission_id' => $firstpermission->id
        ]);

        $secondpermissionrole = factory(PermissionRole::class)->create([
            'role_id' => $role->id,
            'permission_id' => $secondpermission->id
        ]);

        $this->post("/roles/{$role->id}/permissions/store", [
            $firstpermission->id => "1"
        ]);

        $this->assertDatabaseHas('permission_roles', [
           'permission_id' => $firstpermission->id,
           'role_id' => $role->id
        ]);

        $this->assertDatabaseMissing('permission_roles', [
           'permission_id' => $secondpermission->id,
           'role_id' => $role->id
        ]);

        $this->assertCount(2, PermissionRole::all());
    }

    public function test_every_permission_role_belongs_to_a_role()
    {
        $permissionrole = factory(PermissionRole::class)->create();

        $this->assertInstanceOf(Role::class, $permissionrole->role);
    }

    public function test_every_permission_role_belongs_to_a_permission()
    {
        $permissionrole = factory(PermissionRole::class)->create();

        $this->assertInstanceOf(Permission::class, $permissionrole->permission);
    }


}
