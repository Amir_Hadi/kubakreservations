<?php

namespace Tests\Feature;

use App\Permission;
use App\PermissionRole;
use App\Reserve;
use App\Role;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_admin_can_see_create_users_page()
    {
        $role = factory(Role::class)->create();
        $permission = factory(Permission::class)->create(['title_english' => 'add-user']);
        $permissionrole = factory(PermissionRole::class)->create([
            'role_id' => $role->id,
            'permission_id' => $permission->id
        ]);
        $this->actingAs(factory(User::class)->create(['role_id' => $role->id]));
        $this->get('/users/create')->assertStatus(200);
    }

    public function test_admin_can_see_users_index_page()
    {

        $this->loginWithSpecificPermission('read-user');

        $user = factory(User::class)->create();

        $this->get('/users')
            ->assertSee($user->firstname)
            ->assertSee($user->employnumber)
            ->assertSee($user->phone)
            ->assertSee($user->role->title)
        ;

    }

    public function test_created_user_exists_in_database()
    {
        $user = factory(User::class)->create();
        $this->assertDatabaseHas('users', $user->toArray());
    }

    public function test_admin_can_create_users()
    {
        $this->loginWithSpecificPermission('add-user');
        $user = factory(User::class)->make([
            'role_id' => 2,
            'firstname' => 'christian',
            'lastname' => 'bale',
            'employnumber' => '10218',
            'phone' => '09385223393',
            'password' => 'password1234',
            'password_confirmation' => 'password1234'
        ]);
        $this->post(route('users.store'), $user->toArray());
        $this->assertDatabaseHas('users', [
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'employnumber' => $user->employnumber,
        ]);
    }

    public function test_admin_with_permission_can_reserve_for_other_users()
    {
        $this->loginWithSpecificPermission('read-user');
        $user = factory(User::class)->create();
        $reserve = factory(Reserve::class)->create(['user_id' => $user->id]);
        $this->get(route('users.show', $user))
            ->assertSee($reserve->schedule->showDateInJalali())
            ->assertSee($reserve->schedule->dayName())
        ;
    }

    public function test_admin_can_edit_users()
    {
        $this->loginWithSpecificPermission('edit-user');

        $user = factory(User::class)->create();
        $user->firstname = 'new name for user';
        $this->patch(route('users.update', $user), $user->toArray());
        $this->assertDatabaseHas('users', [
            'firstname' => $user->firstname,
            'employnumber' => $user->employnumber,
        ]);

    }

    public function test_admin_can_delete_users()
    {
        $this->loginWithSpecificPermission('delete-user');
        $user = factory(User::class)->create();
        $this->delete(route('users.destroy', $user));
        $this->assertDatabaseMissing('users', [
            'name' => $user->firstname,
            'employnumber' => $user->employnumber
        ]);

    }

    public function test_each_user_has_a_role()
    {
        $user = factory(User::class)->create();
        $this->assertDatabaseMissing('users', ['role_id' => null]);
        $this->assertInstanceOf(Role::class, $user->role);
    }

    public function test_user_firstname_is_required()
    {
        $this->loginWithSpecificPermission('add-user');

        $user = factory(User::class)->make(['firstname' => null]);
        $this->post(route('users.store'), $user->toArray())
            ->assertSessionHasErrors(['firstname']);
        $this->assertDatabaseMissing('users', ['firstname' => null]);
    }

    public function test_user_lastname_is_required()
    {
        $this->loginWithSpecificPermission('add-user');

        $user = factory(User::class)->make(['lastname' => null]);
        $this->post(route('users.store'), $user->toArray())
            ->assertSessionHasErrors(['lastname']);
        $this->assertDatabaseMissing('users', ['lastname' => null]);
    }

    public function test_user_password_is_required()
    {
        $this->loginWithSpecificPermission('add-user');

        $user = factory(User::class)->make(['password' => null]);
        $this->post(route('users.store'), $user->toArray())
            ->assertSessionHasErrors(['password']);
        $this->assertDatabaseMissing('users', ['password' => null]);
    }

    public function test_user_employnumber_is_required()
    {
        $this->loginWithSpecificPermission('add-user');

        $user = factory(User::class)->make(['employnumber' => null]);
        $this->post(route('users.store'), $user->toArray())
            ->assertSessionHasErrors(['employnumber']);
        $this->assertDatabaseMissing('users', ['employnumber' => null]);
    }

    public function test_user_phone_is_required()
    {
        $this->loginWithSpecificPermission('add-user');
        $user = factory(User::class)->make(['phone' => null]);
        $this->post(route('users.store'), $user->toArray())
            ->assertSessionHasErrors(['phone']);
        $this->assertDatabaseMissing('users', ['phone' => null]);
    }

    public function test_authorized_user_can_edit_his_or_her_password()
    {
        $role = factory(Role::class)->create(['title' => 'user']);

        $permission = factory(Permission::class)->create(['title_english' => 'edit-pass']);

        factory(PermissionRole::class)->create(['permission_id' => $permission->id, 'role_id' => $role->id]);

        $user = factory(User::class)->create([
            'role_id' => $role->id,
            'password' => bcrypt('123456')
        ]);


        $this->actingAs($user);

        $this->patch("/users/".$user->id."/updatepassword", [
            'currentpassword' => '123456',
            'password' => '654321',
            'password_confirmation' => '654321'
        ]);

        $this->assertDatabaseMissing('users', [
           'id' => $user->id,
           'password' => bcrypt('123456')
        ]);
    }

    public function test_authorized_user_can_delete_users()
    {
        $this->loginWithSpecificPermission('delete-user');

        $user = factory(User::class)->create();

        $this->delete('/users/' .$user->id);

        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'employnumber'=> $user->emplounumber
        ]);

        $this->assertCount(1, User::all());
    }

    public function test_every_user_has_a_role()
    {
        $user  = factory(User::class)->create();

        $this->assertInstanceOf(Role::class, $user->role);
    }

    public function test_current_password_is_required_for_update_own_password()
    {
        $role = factory(Role::class)->create(['title' => 'user']);

        $permission = factory(Permission::class)->create(['title_english' => 'edit-pass']);

        factory(PermissionRole::class)->create(['permission_id' => $permission->id, 'role_id' => $role->id]);

        $user = factory(User::class)->create([
            'role_id' => $role->id,
            'password' => bcrypt('123456')
        ]);

        $this->actingAs($user);

        $this->patch("/users/".$user->id."/updatepassword", [
            'currentpassword' => null,
            'password' => '654321',
            'password_confirmation' => '654321'
        ])->assertSessionHasErrors('currentpassword');
    }

    public function test_password_is_required_for_update_own_password()
    {
        $role = factory(Role::class)->create(['title' => 'user']);

        $permission = factory(Permission::class)->create(['title_english' => 'edit-pass']);

        factory(PermissionRole::class)->create(['permission_id' => $permission->id, 'role_id' => $role->id]);

        $user = factory(User::class)->create([
            'role_id' => $role->id,
            'password' => bcrypt('123456')
        ]);

        $this->actingAs($user);

        $this->patch("/users/".$user->id."/updatepassword", [
            'currentpassword' => '123456',
            'password' => null,
            'password_confirmation' => '654321'
        ])->assertSessionHasErrors('password');
    }

    public function test_password_confirmation_is_required_for_update_own_password()
    {
        $role = factory(Role::class)->create(['title' => 'user']);

        $permission = factory(Permission::class)->create(['title_english' => 'edit-pass']);

        factory(PermissionRole::class)->create(['permission_id' => $permission->id, 'role_id' => $role->id]);

        $user = factory(User::class)->create([
            'role_id' => $role->id,
            'password' => bcrypt('123456')
        ]);

        $this->actingAs($user);

        $response = $this->patch("/users/".$user->id."/updatepassword", [
            'currentpassword' => '123456',
            'password' => '654321',
            'password_confirmation' => null
        ])->assertSessionHasErrors();

    }

    public function test_authorized_user_can_see_users_edit_password_page()
    {
        $this->loginWithSpecificPermission('edit-user');

        $user = factory(User::class)->create();

        $this->get('/password/'. $user->id . '/edit')->assertStatus(200);
    }

    public function test_authorized_user_can_update_users_password()
    {
        $this->loginWithSpecificPermission('edit-user');

        $password  = Str::random(6);

        $user = factory(User::class)->create([
            'password' => bcrypt($password)
        ]);

        $newpassword = Str::random(6);

        $this->patch('/password/'. $user->id .'/update', [
            'password' => $newpassword,
            'password_confirmation' => $newpassword
        ])->assertRedirect('/users');

        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'password' => $user->password
        ]);
    }
}
