<?php

namespace Tests\Feature;

use App\Role;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoleTest extends TestCase
{
    public function test_authorized_user_can_view_roles()
    {
        $this->loginWithSpecificPermission('read-role');
        $role = factory(Role::class)->create();
        $this->get('/roles')->assertSee($role->title)->assertStatus(200);
    }

    public function test_unauthenticated_user_cant_view_roles()
    {
        $this->get('/roles')->assertStatus(403);
    }

    public function test_unauthorized_user_cant_view_roles()
    {
        $this->loginWithSpecificPermission();
        $this->get('/roles')->assertStatus(403);
    }

    public function test_authorized_user_can_view_role_create_page()
    {
        $this->loginWithSpecificPermission('add-role');

        $this->get('/roles/create')->assertStatus(200);
    }

    public function test_authorized_user_can_create_roles()
    {
        $this->loginWithSpecificPermission('add-role');

        $role = factory(Role::class)->make();

        $this->post('/roles', $role->toArray());

        $this->assertDatabaseHas('roles',[
            'title' => $role->title
        ]);

        $this->assertCount(2, Role::all());
    }

    public function test_title_is_required()
    {
        $this->loginWithSpecificPermission('add-role');

        $role = factory(Role::class)->make(['title' => null]);

        $this->post('/roles', $role->toArray())
            ->assertSessionHasErrors('title');

        $this->assertDatabaseMissing('roles', [
            'title' => null,
        ]);

        $this->assertCount(1, Role::all());
    }

    public function test_authorized_user_can_choose_roles_to_prove_for_the_new_role()
    {
        $this->loginWithSpecificPermission('add-role');

        $role = factory(Role::class)->make();

        $this->post('/roles', $role->toArray());

        $this->assertCount(2, Role::all());

    }

    public function test_authorized_user_can_update_role()
    {
        $this->loginWithSpecificPermission('edit-role');

        $role = factory(Role::class)->create();

        $role->title = 'title has changed';

        $this->patch('/roles/'. $role->id, $role->toArray());

        $this->assertDatabaseHas('roles', [
           'id' => $role->id,
           'title' => $role->title
        ]);

        $this->assertCount(2, Role::all());
    }

    public function test_role_with_no_user_can_be_deleted()
    {
        $this->loginWithSpecificPermission('delete-role');

        $role = factory(Role::class)->create();

        $this->delete('/roles/'. $role->id)->assertRedirect('/roles');

        $this->assertDatabaseMissing('roles', [
           'id' => $role->id,
           'title' => $role->title
        ]);
    }

    public function test_the_role_specified_to_an_user_cant_be_deleted()
    {
        $this->loginWithSpecificPermission('delete-role');

        $role = factory(Role::class)->create();

        factory(User::class)->create(['role_id' => $role->id]);

        $this->delete('/roles/'.$role->id)->assertStatus(403);

        $this->assertDatabaseHas('roles', [
           'id' => $role->id,
           'title' => $role->title
        ]);
    }
}
