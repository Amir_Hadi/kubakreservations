<?php

use Illuminate\Database\Seeder;

class FirstAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = \App\Role::insert([
            'title' => 'admin'
        ]);

        $user = \App\User::insert([
            'firstname' => 'adminuser',
            'lastname' => 'lastname',
            'employnumber' => '10118',
            'phone' => '09181234567',
            'role_id' => 1,
            'password' => bcrypt('password1234'),
        ]);


        \App\Permission::insert([
            'title_english' => 'add-config',
            'title_persian' => 'افزودن پیکربندی',
        ]);
        \App\Permission::insert([
            'title_english' => 'edit-config',
            'title_persian' => 'ویرایش پیکربندی',
        ]);
        \App\Permission::insert([
            'title_english' => 'read-config',
            'title_persian' => 'مشاهده پیکربندی',
        ]);
        \App\Permission::insert([
            'title_english' => 'delete-config',
            'title_persian' => 'حذف پیکربندی',
        ]);

        \App\Permission::insert([
           'title_english' => 'add-user',
           'title_persian' => 'افزودن کاربر',
        ]);
        \App\Permission::insert([
            'title_english' => 'edit-user',
            'title_persian' => 'ویرایش کاربر',
        ]);
        \App\Permission::insert([
            'title_english' => 'edit-pass',
            'title_persian' => 'ویرایش رمز',
        ]);
        \App\Permission::insert([
            'title_english' => 'read-user',
            'title_persian' => 'مشاهده کاربر',
        ]);

        \App\Permission::insert([
            'title_english' => 'delete-user',
            'title_persian' => 'حذف کاربر',
        ]);
        \App\Permission::insert([
            'title_english' => 'add-food',
            'title_persian' => 'افزودن غذا',
        ]);
        \App\Permission::insert([
            'title_english' => 'edit-food',
            'title_persian' => 'ویرایش غذا',
        ]);
        \App\Permission::insert([
            'title_english' => 'read-food',
            'title_persian' => 'مشاهده غذا',
        ]);

        \App\Permission::insert([
            'title_english' => 'delete-food',
            'title_persian' => 'حذف غذا',
        ]);
        \App\Permission::insert([
            'title_english' => 'add-role',
            'title_persian' => 'افزودن نقش',
        ]);
        \App\Permission::insert([
            'title_english' => 'edit-role',
            'title_persian' => 'ویرایش نقش',
        ]);
        \App\Permission::insert([
            'title_english' => 'read-role',
            'title_persian' => 'مشاهده نقش',
        ]);

        \App\Permission::insert([
            'title_english' => 'delete-role',
            'title_persian' => 'حذف نقش',
        ]);
        \App\Permission::insert([
            'title_english' => 'add-schedule',
            'title_persian' => 'افزودن برنامه',
        ]);
        \App\Permission::insert([
            'title_english' => 'edit-schedule',
            'title_persian' => 'ویرایش برنامه',
        ]);
        \App\Permission::insert([
            'title_english' => 'read-schedule',
            'title_persian' => 'مشاهده برنامه',
        ]);

        \App\Permission::insert([
            'title_english' => 'delete-schedule',
            'title_persian' => 'حذف برنامه',
        ]);
        \App\Permission::insert([
            'title_english' => 'add-permissionrole',
            'title_persian' => 'افزودن دسترسی',
        ]);
        \App\Permission::insert([
            'title_english' => 'delete-permissionrole',
            'title_persian' => 'حذف دسترسی',
        ]);
        \App\Permission::insert([
            'title_english' => 'food-reserve',
            'title_persian' => 'رزرو غدا',
        ]);

        \App\Permission::insert([
            'title_english' => 'cancel-food-reserve',
            'title_persian' => 'لغو رزرو',
        ]);

        \App\Permission::insert([
            'title_english' => 'read-log',
            'title_persian' => 'مشاهده وقایع',
        ]);

        for ($i = 1; $i <=26; $i++){
            \App\PermissionRole::insert([
                'permission_id' => $i,
                'role_id' => 1
            ]);
        }


    }
}
