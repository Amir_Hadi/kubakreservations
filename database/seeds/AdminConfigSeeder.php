<?php

use App\AdminConfig;
use Illuminate\Database\Seeder;

class AdminConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminConfig::insert([
            'day' => 3,
            'time' => '12:00 AM'
        ]);
    }
}
