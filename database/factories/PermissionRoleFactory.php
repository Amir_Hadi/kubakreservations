<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Permission;
use App\PermissionRole;
use App\Role;
use Faker\Generator as Faker;


$factory->define(PermissionRole::class, function (Faker $faker) {
    return [
        'permission_id' => factory(Permission::class)->create()->id,
        'role_id' => function(){
            return factory(Role::class)->create()->id;
        }
    ];
});
