<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Food;
use App\Model;
use App\ScheduleFood;
use Faker\Generator as Faker;

$factory->define(ScheduleFood::class, function (Faker $faker) {
    return [
        'food_id' => function(){
            return factory(Food::class)->create()->id;
        },
        'date' => $faker->date('Y-m-d')
    ];
});
