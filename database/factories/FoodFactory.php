<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Http\UploadedFile;

$factory->define(\App\Food::class, function (Faker $faker) {

    Storage::fake();

    return [
        'name' => $faker->name,
        'cost' => $faker->numberBetween(6000, 8000),
        'description' => $faker->paragraph,
        'image' => UploadedFile::fake()->image('food.jpg')
    ];
});



