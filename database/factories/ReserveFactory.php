<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Reserve;
use App\ScheduleFood;
use App\User;
use Faker\Generator as Faker;

$factory->define(Reserve::class, function (Faker $faker) {
    return [
        'schedule_id' => function(){
            return factory(ScheduleFood::class)->create()->id;
        },
        'user_id' => function(){
            return factory(User::class)->create()->id;
        }
    ];
});
