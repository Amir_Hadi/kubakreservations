<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\AdminConfig;
use App\Model;
use Faker\Generator as Faker;

$factory->define(AdminConfig::class, function (Faker $faker) {
    return [
        'day' => $faker->numberBetween(0,6),
        'time' => $faker->time('h:i')
    ];
});
