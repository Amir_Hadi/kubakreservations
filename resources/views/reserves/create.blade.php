@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">رزرو غذای هفته‌ی بعد</h3>
                    <div class="row">
                        <div class="col-md-3">
                            @if($prevweek)
                                <a href="?date={{$prevweek}}" class="btn btn-info btn-sm">&lt هفته قبل </a>
                            @endif
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-3 text-left">
                            @if($nextweek)
                                <a href="?date={{$nextweek}}" class="btn btn-success btn-sm"> هفته بعد &gt</a>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <form action="/reserves" method="post">
                    @csrf
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid"
                                           aria-describedby="example2_info">
                                        <thead>
                                            <tr role="row">
                                                @if($schedules->count() > 0 )
                                                    @foreach($schedules as $schedule)
                                                        <th class="sorting" tabindex="0" aria-controls="example2"
                                                            rowspan="1" colspan="1" ng-class="text-center"
                                                            aria-label="مرورگر: activate to sort column ascending">
                                                            <div class="text-center">
                                                                <p>{{$schedule->dayName()}}</p>
                                                                <p class="text-aqua">{{$schedule->showDateInJalali()}}</p>
                                                            </div>
                                                        </th>
                                                    @endforeach
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if($schedules->count() > 0)
                                            <tr role="row" class="odd">

                                                @csrf
                                                @foreach($schedules as $schedule)
                                                    <td class="text-center">
                                                        @if($schedule->food)
                                                            <p>{{$schedule->food->name}}</p>
                                                            <p> {{$schedule->food->cost}} تومان </p>
                                                        @elseif($schedule->food_id == -1)
                                                            <p class="bg-danger">تعطیل</p>
                                                        @endif

                                                        @if(auth()->user()->reserveAvailable($schedule) && $schedule->food_id != -1)
                                                            <div class="form-group">
                                                                <input type="checkbox"
                                                                       @if(auth()->user()->reserve($schedule))
                                                                       checked
                                                                       @endif
                                                                       class="btn btn-warning"
                                                                       name="reserves[{{$schedule->id}}]">
                                                            </div>
                                                        @else
                                                            @if(auth()->user()->reserve($schedule))
                                                                <p class="bg-success">رزرو شده</p>
                                                            @else
                                                                -
                                                            @endif
                                                        @endif
                                                    </td>
                                                @endforeach


                                            </tr>
                                        @endif
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        @if(auth()->user()->reserveAvailable($schedules->last()))
                            <input type="submit" class="btn btn-primary" value="ارسال">
                        @endif
                    </div>
                </form>
                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

