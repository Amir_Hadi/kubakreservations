@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">رزروهای من</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="موتور رندر: activate to sort column descending">تاریخ</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="مرورگر: activate to sort column ascending">روز</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="سیستم عامل: activate to sort column ascending">غذا</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="ورژن: activate to sort column ascending">رزرو</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="امتیاز: activate to sort column ascending">لغو</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($reserves->count() > 0)
                                            @foreach($reserves as $reserve)
                                                <tr role="row" class="odd">

                                                    <td class="sorting_1">{{$reserve->schedule->showDateInJalali()}}</td>
                                                    <td>{{$reserve->schedule->dayName()}}</td>

                                                    <td>{{$reserve->schedule->food->name}}</td>
                                                    <td class="text-center">
                                                        @if(!(auth()->user()->reserve($reserve->schedule)))
                                                            <input type="checkbox" name="reserve[]" value="{{$reserve->schedule_id}}">
                                                        @else
                                                            <p class="bg-success">رزرو شده</p>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        @if(auth()->check() && auth()->user()->reserve($reserve->schedule))
                                                            {{--                                                            <form action="{{route('resberves.destroy')}}"></form>--}}
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            {{$reserves->links()}}
                                        @endif

                                        </tbody>
                                        <tfoot>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="موتور رندر: activate to sort column descending">تاریخ</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="مرورگر: activate to sort column ascending">روز</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="سیستم عامل: activate to sort column ascending">غذا</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="ورژن: activate to sort column ascending">رزرو</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="امتیاز: activate to sort column ascending">لغو</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-body -->


                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

