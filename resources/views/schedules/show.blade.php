@extends('layouts.main')

@section('content')


    <div class="row">
        <div class="col-md-10">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">رزروهای هفته‌‌ی بعد</h3>
                    <div class="row">
                        <div class="col-md-3">
                            @if($prevweek)
                                <a href="?date={{$prevweek->format('Y-m-d')}}" class="btn btn-info btn-sm">&lt هفته قبل  </a>
                            @endif
                        </div>
                        <div class="col-md-6" class="text-center">
                            @if(request('date'))
                                <a href="/print?date={{request('date')}}" class="btn btn-primary d-inline-block">چاپ برنامه</a>
                            @else
                                <a href="/print" target="_blank" class="btn btn-primary d-inline-block">چاپ برنامه</a>
                            @endif
                        </div>
                        <div class="col-md-3 text-left">
                            @if($nextweek)
                                <a href="?date={{$nextweek->format('Y-m-d')}}" class="btn btn-success btn-sm"> هفته بعد &gt</a>
                            @endif
                        </div>
                    </div>

                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="موتور رندر: activate to sort column descending"></th>
                                        @if($schedules->count() > 0)
                                            @foreach($schedules as $schedule)
                                                <th class="sorting text-center" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="مرورگر: activate to sort column ascending">
                                                    <p class="p-2 mb-3">{{$schedule->dayName()}}</p>
                                                    <p class="p-2 mb-3">{{$schedule->showDateInjalali()}}</p>
                                                    @if($schedule->food)
                                                        <p class="text-aqua">{{$schedule->food->name}}</p>
                                                    @elseif($schedule->food_id == -1)
                                                        <p  style="color: red;">تعطیل</p>
                                                    @else
                                                        <p>-</p>
                                                    @endif
                                                    @can('delete', [auth()->user(), $schedule])
                                                        <form action="/schedules/{{$schedule->id}}/reserve/destroy" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input class="btn btn-warning" onclick="return confirm('آیا مطمئن هستید؟')" type="submit" name="dstroy" id="destroy" value="حذف همه‌ی رزرو ها" >
                                                        </form>
                                                    @endcan
                                                </th>
                                            @endforeach
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($users->count() > 0)
                                        @foreach($users as $user)
                                            <tr role="row" class="odd">

                                                <td>{{$user->fullName()}}</td>

                                                @foreach($schedules as $schedule)
                                                    @if($user->reserve($schedule) && $schedule->food_id != -1)
                                                        <td>
                                                            <p class="text-success bg-success text-center">
                                                                رزرو شده
                                                            </p>
                                                            @can('delete', [auth()->user(), $schedule])

                                                                <form class="text-center" action="/schedules/{{$schedule->id}}/users/{{$user->id}}" method="POST">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <input type="submit" onclick="return confirm('آیا مطمئن هستید؟')" class="btn btn-default" value="لغو">
                                                                </form>

                                                            @endcan

                                                        </td>
                                                    @else
                                                        <td class="text-center">-</td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    @endif

                                    </tbody>
                                    <tfoot>
                                    <tr role="row">
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="مرورگر: activate to sort column ascending">جمع کل روزانه</th>
                                        @foreach($schedules as $schedule)
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="مرورگر: activate to sort column ascending">
                                                <p class="text-center">{{$schedule->reserves->count()}}</p>
                                            </th>
                                        @endforeach
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->


                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>


@endsection
