@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">جدول برنامه ها</h3>
                    <a href="/schedules/create" class="btn btn-success btn-lg">افزودن برنامه جدید</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                    <tr role="row">
                                        @if(count($schedules) > 0)
                                            @foreach($schedules as $schedule)
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="موتور رندر: activate to sort column descending">
                                                    <p class="p-2 mb-3">{{$schedule->dayName()}}</p>
                                                    <p class="p-2 mb-3">{{$schedule->showDateInjalali()}}</p>
                                                    <p class="text-aqua p-2">{{$schedule->food->name}}</p>
                                                </th>
                                            @endforeach
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($schedules->count() > 0)
                                        @foreach($schedules as $schedule)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1">{{$schedule->showDateInJalali()}}</td>
                                                <td>{{$schedule->food->name}}</td>
                                                <td>{{$schedule->reserves->count()}}</td>

                                                <td style="display: inline-flex; flex-direction: row; align-items: center;">
                                                    <a href="/schedules/{{$schedule->id}}/edit" class="btn btn-info btn-lg">ویرایش</a>
                                                    <form action="/schedules/{{$schedule->id}}/destroy" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger btn-lg">حذف</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif

                                    </tbody>
                                    <tfoot>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="موتور رندر: activate to sort column descending">روز</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="مرورگر: activate to sort column ascending">غذا</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="سیستم عامل: activate to sort column ascending">تعداد</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="امتیاز: activate to sort column ascending">کنترل ها </th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                            </div>
                            <div class="col-sm-7">
                                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                                    {{$schedules->links()}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@endsection
