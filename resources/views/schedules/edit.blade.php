@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">ایجاد برنامه جدید</h3>
                </div>
                <form role="form" action="{{route('schedules.update', $schedule) }}" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="box-body">

                        <div class="form-group">
                            <label>روز </label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" disabled name="date" class="form-control pull-right" value="<?= $schedule->date ?>">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="food_id">غذا</label>
                            <select name="food_id" id="food_id" class="form-control">
                                <option value=""  disabled >انتخاب غذا</option>
                                @foreach($foods as $food)
                                    <option value="{{$food->id}}" @if($food->id == $schedule->food_id) selected @endif>{{$food->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">ارسال</button>
                        </div>
                    </div>
                    <!-- /.box-body -->

                </form>
            </div>
            <!-- /.box -->


            <!-- /.box -->
        </div>
        {{--        <div class="col-md-6">--}}
        {{--            <div class="box box-primary">--}}
        {{--                <div class="box-header with-border">--}}
        {{--                    <h3 class="box-title">افزودن غذای جدید</h3>--}}
        {{--                </div>--}}
        {{--                <!-- /.box-header -->--}}
        {{--                <!-- form start -->--}}
        {{--                --}}
        {{--                <form role="form" action="{{route('schedules.store')}}" method="post">--}}
        {{--                    @csrf--}}
        {{--                    <div class="box-body">--}}
        {{--                        <div class="form-group">--}}
        {{--                            <label for="date">تاریخ</label>--}}
        {{--                            <input type="datetime-local" class="form-control" id="date" name="date" placeholder="تاریخ">--}}
        {{--                        </div>--}}
        {{--                        <div class="form-group">--}}
        {{--                            <label>انتخاب تاریخ شمسی با خروجی تایم استمپ </label>--}}
        {{--                            <div class="input-group date">--}}
        {{--                                <div class="input-group-addon">--}}
        {{--                                    <i class="fa fa-calendar"></i>--}}
        {{--                                </div>--}}
        {{--                                <input type="text" id="tarikh" class="form-control pull-right">--}}
        {{--                                <input type="text" id="tarikhAlt" class="form-control pull-right">--}}
        {{--                            </div>--}}
        {{--                            <!-- /.input group -->--}}
        {{--                            <br>--}}
        {{--                            <p>استفاده از کتابخانه باباخانی، برای تغییرات <a href="http://babakhani.github.io/PersianWebToolkit/doc/datepicker/">مستندات این کتابخانه</a> را مشاهده کنید </p>--}}
        {{--                        </div>--}}
        {{--                        <div class="form-group">--}}
        {{--                            <label for="food_id">غذا</label>--}}
        {{--                            <select name="food_id" id="food_id" class="form-control">--}}
        {{--                                <option value="" selected disabled >انتخاب غذا</option>--}}
        {{--                                @foreach($foods as $food)--}}
        {{--                                    <option value="{{$food->id}}">{{$food->name}}</option>--}}
        {{--                                @endforeach--}}
        {{--                            </select>--}}
        {{--                        </div>--}}
        {{--                        <div class="form-group">--}}
        {{--                            <label for="image">ارسال تصویر</label>--}}
        {{--                            <input type="file" id="image" name="image" value="تصویر">--}}

        {{--                            <p class="help-block">متن راهنما</p>--}}
        {{--                        </div>--}}

        {{--                    </div>--}}
        {{--                    <div class="form-group">--}}
        {{--                        <label for="description">توضیحات</label>--}}
        {{--                        <textarea id="description" name="description" class="form-control" rows="3" placeholder="توضیحات"></textarea>--}}
        {{--                    </div>--}}
        {{--                    <!-- /.box-body -->--}}

        {{--                    <div class="box-footer">--}}
        {{--                        <button type="submit" class="btn btn-primary">ارسال</button>--}}
        {{--                    </div>--}}
        {{--                </form>--}}

        {{--                @if($errors->count() > 0)--}}
        {{--                    <div class="form-group has-error">--}}
        {{--                        @foreach($errors->all() as $error)--}}
        {{--                            <span class="help-block">{{$error}}</span>--}}
        {{--                        @endforeach--}}
        {{--                    </div>--}}
        {{--                @endif--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>

@endsection
