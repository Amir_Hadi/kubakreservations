@extends('layouts.main')

@section('content')
    @php
        $now = Carbon\Carbon::now()->startOfWeek(\Carbon\Carbon::SATURDAY)->addWeek();
        if(request('date')){
            $firstday = Carbon\Carbon::parse(request('date'))->startOfWeek(\Carbon\Carbon::SATURDAY);
            $lastday = Carbon\Carbon::parse(request('date'))->startOfWeek(\Carbon\Carbon::SATURDAY)->endOfWeek(\Carbon\Carbon::FRIDAY);
        } else {
            $firstday = Carbon\Carbon::now()->startOfWeek(\Carbon\Carbon::SATURDAY)->addWeek();
            $lastday = Carbon\Carbon::now()->startOfWeek(\Carbon\Carbon::SATURDAY)->addWeek()->endOfWeek(\Carbon\Carbon::FRIDAY);
        }
    @endphp
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">ایجاد برنامه</h3>
                    <div class="row">
                        <div class="col-md-3">
                            @if($prevweek)
                                <a href="?date={{$prevweek->format('Y-m-d')}}" class="btn btn-warning btn-sm">&lt هفته قبل  </a>
                            @endif
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-3 text-left">
                            <a href="?date={{$nextweek->format('Y-m-d')}}" class="btn btn-success btn-sm"> هفته بعد &gt</a>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="/schedules/store" method="post">
                                    @csrf
                                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr role="row">

                                                @while(!$firstday->greaterThanOrEqualTo($lastday))
                                                    <th class="text-center">
                                                        <p>{{\App\Helper\Helper::toJalali($firstday, 'l')}}</p>
                                                        <p class="text-aqua">{{\App\Helper\Helper::toJalali($firstday, 'Y-m-d')}}</p>
                                                    </th>
                                                    @php $firstday->addDay(); @endphp
                                                @endwhile
                                                @php $firstday->subWeek() @endphp

                                            </tr>
                                        </thead>


                                        <tbody>
                                            <tr>
                                                @while(!$firstday->greaterThanOrEqualTo($lastday))
                                                    <td class="text-center">
                                                        @php
                                                            $schedule = \App\ScheduleFood::where('date', \Carbon\Carbon::parse($firstday)->format('Y-m-d'))->get()->first();
                                                        @endphp
                                                        @if(
                                                            (!empty($schedule) && !\Carbon\Carbon::parse($schedule->date)->isPast()) ||
                                                            (\Carbon\Carbon::parse($firstday)->greaterThan(\Carbon\Carbon::now()))
                                                        )
                                                            <select name="schedules[{{\Carbon\Carbon::parse($firstday)->format('Y-m-d')}}]" id="schedules[{{$firstday}}]" class="form-control">
                                                            <option value="" disabled selected>انتخاب غذا</option>
                                                            @foreach($foods as $food)
                                                                <option value="{{$food->id}}"

                                                                @if($schedule && $schedule->food_id == $food->id)
                                                                    selected="selected"
                                                                @endif
                                                                > {{$food->name}} </option>
                                                            @endforeach

                                                            <option @if($schedule && $schedule->food_id == -1)
                                                                    selected="selected"
                                                                    @endif value="-1">تعطیل</option>

                                                        </select>
                                                        @else
                                                            @if(!empty($schedule))
                                                                <p>{{$schedule->food->name}}</p>
                                                            @else
                                                                -
                                                            @endif
                                                        @endif
                                                    </td>
                                                    @php $firstday->addDay(); @endphp
                                                @endwhile
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">ارسال</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->

                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
