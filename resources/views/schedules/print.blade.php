<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body{
            font-family: DejaVu Sans, sans-serif;
        }
    </style>
</head>
<body dir="rtl">
<style>
    @page{
        size:auto;
        margin:0mm;
    }
    .container {
        margin: auto;
        width: 95%;
    }

    .header_inner {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    .header_inner .text {
        text-align: center;
    }
    table{
        width: 100%;
        border: 2px solid black;
        font-size: 8px;
    }
    td {
        border: 1px solid black;
    }
    td p {
        text-align: center;
    }

    tbody td {
        text-align: center;
    }

    .header, .username {
        border-width: 2px;
        font-weight: bold;
    }
    .footer td {
        background: #f5f5f5;
        font-weight: bold;
        border: 2px solid black;
    }
</style>

    <div class="container">
        <div class="header">
            <div class="header_inner">
                <div class="logo">
                    <small>لوگوی کوباک</small>
                </div>
                <div class="text">
                    <h5>لیست رزرواسیون هفتگی شرکت کوباک</h5>
                    <h6>یک متن دیگر نیز می‌تواند اینجا قرار گیرد</h6>
                </div>
                <div class="date">
                    <small>تاریخ گزارش گیری:  <span dir="ltr">{{\App\Helper\Helper::toJalali(\Carbon\Carbon::now())}}</span></small>
                </div>
            </div>
        </div>
        <table>
            <thead>
            <tr>
                <td></td>
                @foreach($schedules as $schedule)
                    <td class="header">
                        <p>{{$schedule->dayName()}}</p>
                        <p>{{$schedule->showDateInJalali()}}</p>
                        <p>{{$schedule->food->name}}</p>
                    </td>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td class="username">{{$user->fullName()}}</td>
                    @foreach($schedules as $schedule)

                        @if($user->reserve($schedule))
                            <td>
                                <p>رزرو شده</p>
                            </td>
                        @else
                            <td>-</td>
                        @endif

                    @endforeach
                </tr>
            @endforeach
            <tr class="footer">
                <td>جمع کل روزانه</td>
                @foreach($schedules as $schedule)
                    <td>
                        <p>{{$schedule->reserves->count()}}</p>
                    </td>
                @endforeach
            </tr>
            </tbody>



        </table>

    </div>
</body>
<script type="text/javascript">
    window.onload = function() { window.print(); }


</script>
</html>
