@extends('layouts.main')

@section('content')
    <style>
        .checkboxes {
            display: flex;
            flex-direction: row;
            align-items: flex-start;
            justify-content: space-around;
        }
    </style>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">افزودن دسترسی جدید به نقش {{$role->title}}</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="/roles/{{$role->id}}/permissions/store" method="POST">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="title">عنوان نقش</label>
                            <input type="text" disabled class="form-control" id="role" name="role" value="{{$role->title}}">
                        </div>

                        <hr>

                        <div class="form-group">
                            <label class="text-aqua">دسترسی کاربران</label>
                            <br>
                            <div class="checkboxes">
                                @foreach($users as $user)
                                    <label>
                                        <input type="checkbox" @if(in_array($user->id, $permissionroles)) checked @endif  name="{{$user->id}}">{{$user->title_persian}}
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <hr>
                        <div class="form-group">
                            <label class="text-aqua">دسترسی تغییر رمز عبور کاربران</label>
                            <br>
                            <div class="checkboxes">
                                @foreach($pass as $pas)
                                    <label>
                                        <input type="checkbox" @if(in_array($pas->id, $permissionroles)) checked @endif name="{{$pas->id}}">{{$pas->title_persian}}
                                    </label>
                                @endforeach

                            </div>
                        </div>


                        <hr>
                        <div class="form-group">
                            <label class="text-aqua">دسترسی غذا</label>
                            <br>
                            <div class="checkboxes">
                                @foreach($foods as $food)
                                    <label>
                                        <input type="checkbox" @if(in_array($food->id, $permissionroles)) checked @endif name="{{$food->id}}">{{$food->title_persian}}
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <hr>
                        <div class="form-group">
                            <label class="text-aqua">دسترسی نقش ها</label>
                            <br>
                            <div class="checkboxes">
                                @foreach($roles as $role)
                                    <label>
                                        <input type="checkbox" @if(in_array($role->id, $permissionroles)) checked @endif name="{{$role->id}}">{{$role->title_persian}}
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <hr>
                        <div class="form-group">
                            <label class="text-aqua">دسترسی برنامه ها</label>
                            <br>
                            <div class="checkboxes">
                                @foreach($schedules as $schedule)
                                    <label>
                                        <input type="checkbox" @if(in_array($schedule->id, $permissionroles)) checked @endif name="{{$schedule->id}}">{{$schedule->title_persian}}
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <hr>
                        <div class="form-group">
                            <label class="text-aqua">دسترسی پیکربندی</label>
                            <br>
                            <div class="checkboxes">
                                @foreach($configs as $config)
                                    <label>
                                        <input type="checkbox" @if(in_array($config->id, $permissionroles)) checked @endif name="{{$config->id}}">{{$config->title_persian}}
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <hr>
                        <div class="form-group">
                            <label class="text-aqua">دسترسی رزروها</label>
                            <br>
                            <div class="checkboxes">
                                @foreach($reserves as $reserve)
                                    <label>
                                        <input type="checkbox" @if(in_array($reserve->id, $permissionroles)) checked @endif name="{{$reserve->id}}">{{$reserve->title_persian}}
                                    </label>
                                @endforeach

                            </div>
                        </div>

                        <hr>
                        <div class="form-group">
                            <label class="text-aqua">دسترسی دسترسی‌ها</label>
                            <br>
                            <div class="checkboxes">
                                @foreach($permissions as $permission)
                                    <label>
                                        <input type="checkbox" @if(in_array($permission->id, $permissionroles)) checked @endif name="{{$permission->id}}">{{$permission->title_persian}}
                                    </label>
                                @endforeach
                            </div>
                        </div>

                    </div>


                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>

                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
