@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">افزودن غذای جدید</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{route('foods.update', $food)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="text-danger"><small>*</small>عنوان غذا</label>
                            <input type="name" class="form-control" id="name" name="name" value="{{$food->name}}">
                        </div>
                        <div class="form-group">
                            <label for="cost" class="text-danger"><small>*</small>هزینه</label>
                            <input type="text" class="form-control" id="cost" name="cost" placeholder="هزینه" value="{{$food->cost}}">
                        </div>
                        <div class="form-group">
                            <label for="image">ارسال تصویر</label>
                            <input type="file" id="image" name="image" value="تصویر">

                        </div>

                    </div>
                    <div class="form-group">
                        <label for="description" class="text-danger"><small>*</small> توضیحات</label>
                        <textarea id="description" name="description" class="form-control" rows="3" placeholder="توضیحات">{{$food->description}}
                        </textarea>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>

                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
