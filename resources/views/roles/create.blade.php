@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">افزودن نقش جدید</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{route('roles.store')}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="title" class="text-danger"> <small>*</small>عنوان</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="عنوان نقش" value="{{old('title')?? ''}}">
                        </div>

                        <div class="form-group">
                            <label for="rolesToProve[]">انتخاب نقش ها</label>
                            @foreach($roles as $role)
                                <label>
                                    <input type="checkbox" name="rolesToProve[{{$role->id}}]" value="{{$role->id}}">{{$role->title}}
                                </label>
                            @endforeach
                        </div>
                    </div>

                    <!-- /.box-body -->



                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>

                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
