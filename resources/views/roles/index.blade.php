@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">جدول نقش ها</h3>
                    @can('create', [\App\Role::class, auth()->user()])
                        <a href="{{route('roles.create')}}" class="btn btn-success">افزودن نقش جدید</a>
                    @endcan
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="مرورگر: activate to sort column ascending">عنوان</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="ورژن: activate to sort column ascending">دسترسی ها</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="امتیاز: activate to sort column ascending">کنترل ها</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($roles->count() > 0)
                                        @foreach($roles as $role)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1">{{$role->title}}</td>
                                                <td>
                                                    @foreach($role->permissions as $permission)
                                                        @php
                                                            $permissionrole = \App\PermissionRole::select('*')->where('role_id', $role->id)->where('permission_id', $permission->id)->get()->first();
                                                        @endphp
                                                        @can('delete', [auth()->user(), $permissionrole])
                                                            <form action="/permissionroles/{{$permissionrole->id}}/destroy" method="POST" style="display: inline-block;">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit" onclick="return confirm('آیا مطمئن هستید؟')" class="btn bg-orange margin" value="{{$permission->id}}">{{$permission->title_persian}}</button>
                                                            </form>
                                                        @else
                                                            <button type="button" class="btn bg-orange margin">{{$permission->title_persian}}</button>
                                                        @endcan
                                                    @endforeach
                                                </td>
                                                <td class="td-controls">
                                                    @can('update', [$role, auth()->user()])
                                                        <a href="{{route('roles.edit', $role)}}" class="btn btn-info">ویرایش</a>
                                                    @endcan
                                                    @can('create', [\App\PermissionRole::class, auth()->user()])
                                                        <a href="/roles/{{$role->id}}/permissions" class="btn btn-success">افزودن دسترسی</a>
                                                    @endcan
                                                    @can('delete', [$role, auth()->user()])
                                                        <form action="{{route('roles.destroy', $role)}}" method="post">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" onclick="return confirm('آیا مطمئن هستید؟')" class="btn btn-danger">حذف</button>
                                                        </form>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif

                                    </tbody>
                                    <tfoot>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="موتور رندر: activate to sort column descending">عنوان</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="ورژن: activate to sort column ascending">دسترسی ها</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="امتیاز: activate to sort column ascending">کنترل ها</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                                    {{$roles->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@endsection
