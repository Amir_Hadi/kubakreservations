<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <style>
        .td-controls{
            display: inline-flex;
            justify-content: space-around;
            width: 100%;
            flex-direction: row;
            align-items: center;
        }
    </style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>صفحه شروع | کنترل پنل</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/dist/css/bootstrap-theme.css">
    <!-- Bootstrap rtl -->
    <link rel="stylesheet" href="/dist/css/rtl.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="/dist/css/persian-datepicker-0.4.5.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="/dist/css/skins/skin-blue.min.css">

    <link rel="stylesheet" href="/css/persianDatepicker-default.css">

    <link rel="stylesheet" href="/bower_components/select2/dist/css/select2.min.css">

    <link rel="stylesheet" href="/plugins/timepicker/bootstrap-timepicker.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">پنل</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b> پنل {{auth()->user()->role->title}}</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <a class="d-inline-block text-left">
                <form action="/logout" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="submit" name="logout" id="logout" value="خروج" class="btn btn-danger" style="border-top: none;border-top-left-radius: 0;border-top-right-radius: 0;padding-top: 15px; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">
                </form>
            </a>

        </nav>
    </header>
    <!-- right side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel" style="height: 65px;">
                @auth
                    <div class="pull-right info">
                        <p>{{auth()->user()->fullName()}}</p>
                        <!-- Status -->
                        <a><i class="fa fa-circle text-success"></i>آنلاین</a>
                    </div>
                @endauth
            </div>


            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">بخش ها</li>
                <!-- Optionally, you can add icons to the links -->
                @can('viewAny', [\App\AdminConfig::class, auth()->user()])
                    <li><a href="/configs"><i class="fa fa-link"></i> <span>تنظیمات</span></a></li>
                @endcan
                @can('viewAny', [\App\Log::class, auth()->user()])
                    <li><a href="/logs"><i class="fa fa-link"></i> <span>ثبت وقایع</span></a></li>
                @endcan
                @can('updatePassword', [auth()->user(), auth()->user()])
                    <li><a href="/users/{{auth()->id()}}/editpassword"><i class="fa fa-link"></i> <span>ویرایش رمز</span></a></li>
                @endcan
                @can('viewAny', [\App\Food::class, auth()->user()])
                    <li class="treeview">
                        <a href="#"><i class="fa fa-link"></i> <span>مدیریت غذاها</span>
                            <span class="pull-left-container">
                             <i class="fa fa-angle-right pull-left"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{route('foods.index')}}">نمایش غذاها</a></li>
                            @can('create', [\App\Food::class, auth()->user()])
                                <li><a href="{{route('foods.create')}}">افزودن غذا</a></li>
                            @endcan
                        </ul>
                    </li>

                @endcan

                @can('viewAny', [auth()->user(), \App\User::class])
                    <li class="treeview">
                        <a href="#"><i class="fa fa-link"></i> <span>مدیریت کاربران</span>
                            <span class="pull-left-container">
                                <i class="fa fa-angle-right pull-left"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{route('users.index')}}">نمایش کاربران</a></li>
                            @can('create', [\App\User::class, auth()->user()])
                                <li><a href="{{route('users.create')}}">افزودن کاربر</a></li>
                            @endcan
                        </ul>
                    </li>

                @endcan



                @can('viewAny', [\App\Access::class, auth()->user()])
                    <li class="treeview">
                        <a href="#"><i class="fa fa-link"></i> <span>مدیریت دسترسی ها</span>
                            <span class="pull-left-container">
                                <i class="fa fa-angle-right pull-left"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{route('accesses.index')}}">نمایش دسترسی ها</a></li>
                            @can('create', [\App\Access::class, auth()->user()])
                                <li><a href="{{route('accesses.create')}}">افزودن دسترسی</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('viewAny', [\App\Role::class, auth()->user()])
                    <li class="treeview">
                        <a href="#"><i class="fa fa-link"></i> <span>مدیریت نقش ها</span>
                            <span class="pull-left-container">
                            <i class="fa fa-angle-right pull-left"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{route('roles.index')}}">نمایش نقش ها</a></li>
                            @can('create', [\App\Role::class, auth()->user()])
                                <li><a href="{{route('roles.create')}}">افزودن نقش</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('viewAny', [\App\ScheduleFood::class, auth()->user()])

                    <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>مدیریت برنامه‌ ها</span>
                        <span class="pull-left-container">
                            <i class="fa fa-angle-right pull-left"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">

                        @can('create', [App\ScheduleFood::class, auth()->user()])
                            <li>
                                <a href="/schedules/create">افزودن برنامه</a>
                            </li>
                            <li>
                                <a href="/schedules/shownextweek">نمایش رزروهای هفتگی</a>
                            </li>
                        @endcan
                    </ul>
                </li>
                @endcan
                @can('viewAny', [\App\Reserve::class, auth()->user()])
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>مدیریت رزرو ها</span>
                        <span class="pull-left-container">
                            <i class="fa fa-angle-right pull-left"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @can('viewAny', [\App\Reserve::class, auth()->user()])
                            <li><a href="/reserves/create">رزرو هفته‌ی بعد</a></li>
                        @endcan
                    </ul>

                </li>
                @endcan

            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        @if(session()->has("success"))
            <row>
                <div class="col-sm-4">
                    <div class="callout callout-success">
                        <h4>اعلان</h4>

                        <p>{{session()->get("success")}}</p>
                    </div>
                </div>
            </row>
        @elseif(session()->has('delete'))
            <row>
                <div class="col-sm-4">
                    <div class="callout callout-info">
                        <h4>اطلاع</h4>

                        <p>{{session()->get("delete")}}</p>
                    </div>
                </div>
            </row>
        @elseif(session()->has('danger'))
            <row>
                <div class="col-sm-4">
                    <div class="callout callout-danger">
                        <h4>اخطار</h4>

                        <p>{{session()->get("danger")}}</p>
                    </div>
                </div>
            </row>
        @endif
        <!-- Main content -->
        <section class="content container-fluid">

            @yield('content')

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->



    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

{{--<script src="/bower_components/jquery/dist/jquery.min.js"></script>--}}
{{--<!-- Bootstrap 3.3.7 -->--}}
{{--<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--}}
{{--<!-- Select2 -->--}}
<script src="/bower_components/select2/dist/js/select2.full.min.js"></script>


<!-- jQuery 3 -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.min.js"></script>
<script src="/dist/js/persian-date.js"></script>
<script src="/dist/js/persian-date-0.1.8.min.js"></script>
<script src="/dist/js/persian-datepicker-0.4.5.min.js"></script>
<script src="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="/bower_components/fastclick/lib/fastclick.js"></script>
<script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>
{{--<script src="/js/persianDatepicker.min.js"></script>--}}
<script>

    $(document).ready(function () {
        $('#tarikh').persianDatepicker({
            initialValue: false,
            format: 'YYYY-MM-D',
            'altField': '#dateAlt',
            'altFormat': 'X',
            observer: true,
            timePicker: {
                enabled: false
            },
            checkDate: function(unix){
                return new persianDate(unix).day() != 6;
            },
            viewMode: 'month',
        });

        $('#date').persianDatepicker({
            initialValue: false,
            format: 'YYYY-MM-D',
            observer: true,
            timePicker: {
                enabled: false
            },
            viewMode: 'month',
        });

        $('.timepicker').timepicker({
            showInputs: false,
            icons: {
                up: "fa fa-angle-up",
                down: "fa  fa-angle-down",
            },
            setTime: '10:45 PM'
        });
    });
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate  : moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass   : 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: true
        })
    })
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>
