@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">افزودن کاربر</h3>
                    @include('layouts.formrequiredfields')
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{route('users.store')}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="firstname" class="text-danger"><small>*</small>نام</label>
                            <input type="text" class="form-control" name="firstname" id="firstname" placeholder="نام" value="{{old('firstname')??''}}">
                        </div>
                        <div class="form-group">
                            <label for="lasttname" class="text-danger"><small>*</small>نام خانوادگی</label>
                            <input type="text" class="form-control" name="lasttname" id="lasttname" placeholder="نام خانوادگی" value="{{old('lasttname')??''}}">
                        </div>
                        <div class="form-group">
                            <label for="employnumber" class="text-danger"><small>*</small>شماره کارمندی</label>
                            <input type="number" class="form-control" name="employnumber" id="employnumber" placeholder="شماره کارمندی" value="{{old('employnumber')??''}}">
                        </div>
                        <div class="form-group">
                            <label for="phone" class="text-danger"><small>*</small>شماره موبایل</label>
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="شماره موبایل" value="{{old('phone')??''}}">
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-danger"><small>*</small>رمز عبور</label>
                            <input type="password" class="form-control" name="password" id="password">
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation" class="text-danger"><small>*</small> تکرار رمز عبور</label>
                            <input type="password" class="form-control" name="password_confirmation" id="password_confiramtion">
                        </div>
                        <div class="form-group">
                            <label for="role_id" class="text-danger"><small>*</small>نقش</label>
                            <select type="text" class="form-control" id="role_id" name="role_id" placeholder="عنوان نقش">
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->title}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>

                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

