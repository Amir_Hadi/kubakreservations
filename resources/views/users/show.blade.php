@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> رزروهای {{$user->fullName()}}</h3>

                </div>
                <!-- /.box-header -->
                <!-- form start -->
{{--                <form role="form" action="{{route('reserves.store')}}" method="post">--}}
{{--                    @csrf--}}
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="موتور رندر: activate to sort column descending">تاریخ</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="مرورگر: activate to sort column ascending">روز</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="سیستم عامل: activate to sort column ascending">غذا</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="ورژن: activate to sort column ascending">رزرو</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($reserves->count() > 0)
                                            @foreach($reserves as $reserve)
                                                <tr role="row" class="odd">
                                                    <td class="sorting_1">{{$reserve->schedule->showDateInJalali()}}</td>
                                                    <td>{{$reserve->schedule->dayName()}}</td>

                                                    <td>{{$reserve->schedule->food->name}}</td>
                                                    <td class="text-center">
                                                        @if(!($user->reserve($reserve->schedule)))
                                                            -
                                                        @else
                                                            <p class="bg-success">رزرو شده</p>
                                                        @endif
                                                    </td>

                                                </tr>
                                            @endforeach
                                        @endif

                                        </tbody>
                                        <tfoot>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="موتور رندر: activate to sort column descending">تاریخ</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="مرورگر: activate to sort column ascending">روز</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="سیستم عامل: activate to sort column ascending">غذا</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="ورژن: activate to sort column ascending">رزرو</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-body -->



                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">مشاهده آمار کاربر {{$user->name}}</h3>
                </div>
                <div class="box-body">

                    <form action="{{route('users.show', $user)}}">
                        <div class="form-group">
                            <label>از تاریخ </label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="tarikh" name="startdate" class="form-control pull-right" value="{{request('startdate')??''}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label>تا تاریخ </label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="date" name="enddate" class="form-control pull-right" value="{{request('enddate')??''}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="جستجو" class="btn btn-adn">
                        </div>
                    </form>



                </div>
                <!-- /.box-body -->
            </div>
            <div class="col-md-6">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">رزرو ها</span>
                        <span class="info-box-number">{{$reserves->count()}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-usd"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"> هزینه رزروها </span>
                        @php
                            $total = 0;
                            foreach($reserves as $reserve){
                                $total = $total + $reserve->schedule->food->cost;
                            }
                        @endphp
                        <span class="info-box-number">{{$total}} تومان </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>
        </div>
    </div>
{{--    <div class="row">--}}
{{--        --}}
{{--    </div>--}}
@endsection

