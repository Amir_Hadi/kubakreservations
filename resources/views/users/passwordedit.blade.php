
@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">ویرایش رمز عبور</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="/password/{{$user->id}}/update" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="box-body">

                        <div class="form-group">
                            <label for="password">رمز عبور جدید</label>
                            <input type="password" class="form-control" name="password" id="password">
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation"> تکرار رمز عبور جدید</label>
                            <input type="password" class="form-control" name="password_confirmation" id="password_confiramtion">
                        </div>

                    </div>

                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>

                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

