@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> ویرایش اطلاعات کاربر {{$user->fullName()}} </h3>
                    @include('layouts.formrequiredfields')
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{route('users.update', $user)}}" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="box-body">
                        <div class="form-group">
                            <label for="firstname" class="text-danger"><small>*</small>نام</label>
                            <input type="text" class="form-control" name="firstname" id="firstname" placeholder="نام" value="{{$user->firstname}}">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="text-danger"><small>*</small>نام خانوادگی</label>
                            <input type="text" class="form-control" name="lastname" id="lastname" placeholder="نام خانوادگی" value="{{$user->lastname}}">
                        </div>
                        <div class="form-group">
                            <label for="employnumber" class="text-danger"><small>*</small>شماره کارمندی</label>
                            <input type="number" class="form-control" name="employnumber" id="employnumber" value="{{$user->employnumber}}">
                        </div>
                        <div class="form-group">
                            <label for="phone" class="text-danger"><small>*</small>شماره موبایل</label>
                            <input type="text" class="form-control" name="phone" id="phone" value="{{$user->phone}}">
                        </div>

                        <div class="form-group">
                            <label for="role_id" class="text-danger"><small>*</small>نقش</label>
                            <select type="text" class="form-control" id="role_id" name="role_id" placeholder="عنوان نقش">
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}" @if($user->role_id == $role->id) selected @endif>{{$role->title}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>

                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
