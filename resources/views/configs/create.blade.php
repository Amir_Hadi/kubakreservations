@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">افزودن پیکربندی جدید</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{route('configs.store')}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="day">روز</label>
                            <select name="day" id="day" class="form-control">
                                <option value="1">شنبه</option>
                                <option value="2">یک شنبه</option>
                                <option value="3">دو شنبه</option>
                                <option value="4">سه شنبه</option>
                                <option value="5">جهار شنبه</option>
                                <option value="6">پنج شنبه</option>
                                <option value="0">جمعه</option>
                            </select>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="bootstrap-timepicker">
                            <div class="form-group">
                                <label>انتخاب زمان</label>

                                <div class="input-group">
                                    <input type="text" class="form-control timepicker" name="time" id="time">

                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                        </div>
                    </div>

                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>

                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
