@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">ویرایش پیکربندی</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
{{--                @php--}}
{{--                    dd($adminConfig);--}}
{{--                @endphp--}}
                <form role="form" action="{{route('configs.update', $config)}}" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="box-body">
                        <div class="form-group">
                            <label for="day">روز</label>
                            <select name="day" id="day" class="form-control">
                                <option value="1" @if($config->day == 1) selected @endif>شنبه</option>
                                <option value="2" @if($config->day == 2) selected @endif>یک شنبه</option>
                                <option value="3" @if($config->day == 3) selected @endif>دو شنبه</option>
                                <option value="4" @if($config->day == 4) selected @endif>سه شنبه</option>
                                <option value="5" @if($config->day == 5) selected @endif>جهار شنبه</option>
                                <option value="6" @if($config->day == 6) selected @endif>پنج شنبه</option>
                                <option value="0" @if($config->day == 0) selected @endif>جمعه</option>
                            </select>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="bootstrap-timepicker">
                            <div class="form-group">
                                <label>انتخاب زمان</label>

                                <div class="input-group">
                                    <input type="text" class="form-control timepicker" name="time" id="time">

                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                        </div>
                    </div>

                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>

                @if($errors->count() > 0)
                    <div class="form-group has-error">
                        @foreach($errors->all() as $error)
                            <span class="help-block">{{$error}}</span>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
