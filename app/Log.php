<?php

namespace App;

use App\Helper\Helper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected  $guarded = [];

    protected $dates = [
        'date',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    public static function log($table, $action, $description)
    {
        $date = Helper::toJalali(Carbon::now());
        $time = Carbon::now()->format('H:i');

//        $description = sprintf(" کاربر %s عمل %s را بر جدول %s در تاریخ <span class='d-inline-block' dir='rtl'>%s</span> و ساعت  <span class='d-inline-block' dir='rtl'>%s</span> انجام داده است", auth()->user()->name, $action, $table, $date, $time );

        Log::insert([
            'user_id' => auth()->user()->id,
            'action' => $action,
            'table' => $table,
            'date' => Carbon::now(),
            'time' => $time,
            'description' =>  $description,
        ]);
    }
}
