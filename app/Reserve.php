<?php

namespace App;

use App\Helper\Helper;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function schedule()
    {
        return $this->belongsTo(ScheduleFood::class, 'schedule_id');
    }

    public function isAvailabelToReserve(ScheduleFood $schedule)
    {
        $dayOfWeek = $schedule->date->dayOfWeek;

        if($dayOfWeek == 0){
            $dayOfWeek = 7;
        }
        $scheduledate = $schedule->date->startOfWeek(Carbon::SATURDAY);
        $nextweek = Carbon::now()->startOfWeek(Carbon::SATURDAY)->addWeek();

        return $scheduledate->equalTo($nextweek) &&
            ( $dayOfWeek < AdminConfig::getDay() ||
                (
                    $dayOfWeek == AdminConfig::getDay() &&
                    Carbon::parse(Carbon::now()->format('H:i'))->lessThan(Carbon::parse(AdminConfig::gettime()))
                )
            );

    }

    public static function exists(User $user,  $schedule)
    {
        return Reserve::where([['user_id', auth()->id()], ['schedule_id', $schedule]])->get()->count() == 1;
    }

    public static function weeklyReserves(User $user, $date = null)
    {
        $startofweek = null;
        if($date == null){
            $startofweek = Helper::nextWeek();
        }else{
            $startofweek = Carbon::parse($date)->startOfWeek(Carbon::SATURDAY);
        }

        $schedulesId = array_column(ScheduleFood::weeklySchedules($startofweek)->select('id')->get()->toArray(), 'id');
//        $reserves = array_column(Reserve::where('user_id', $user->id)->whereIn('schedule_id', $schedulesId)->select('id')->get()->toArray(), 'id');
        return $schedulesId;
    }


}
