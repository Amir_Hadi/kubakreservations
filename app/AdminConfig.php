<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AdminConfig extends Model
{
    protected $fillable = ['day', 'time'];

    private $days = ['جمعه', 'شنبه','یکشنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنج شنبه' ];

    public static function dayTime()
    {
        return self::getDay() . self::getTime();
    }

    public static function getDay()
    {
        return (integer)self::first()->day;
    }

    public static function getTime()
    {
        return self::first()->time;
    }

    public function dayName()
    {
        return $this->days[$this->day];
    }

    public static function getHour()
    {
        if(preg_match("/ AM/", self::getTime())){
            return explode(':', self::getTime())[0];
        }
        return explode(':', self::getTime())[0] + 12;
    }

    public static function getMinute()
    {
        if(preg_match("/ AM/", self::getTime())){
            return explode(':', preg_replace("/ AM/", "",self::getTime()))[1];
        }
        return explode(':', preg_replace("/ PM/", "",self::getTime()))[1];


    }


}
