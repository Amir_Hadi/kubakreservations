<?php


namespace App\Helper;


use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Support\Facades\DB;

class Helper
{
    public static function dbTables()
    {
        $tables = DB::select('SHOW TABLES');
        $tablesArray = [];
        $notNeccessaryTables = ['password_resets', 'migrations', 'failed_jobs'];
        foreach($tables as $table){
            $tablesArray[] = array_values(json_decode(json_encode($table), true))[0];
        }
        $tablesArray = array_diff($tablesArray, $notNeccessaryTables);
        return $tablesArray;
    }

    public static function toGregorian($date, $format = 'Y-m-d')
    {
        $date = Verta::getGregorian($date->year, $date->month, $date->day);
        return Carbon::parse("$date[0]-$date[1]-$date[2]");
    }

    public static function toJalali($date, $format = 'Y-m-d')
    {
        $date = Verta::getJalali($date->year, $date->month, $date->day);
        return Verta::parse("$date[0]-$date[1]-$date[2]")->format($format);
    }

    public function toJalalidate($date){
        return self::toJalali($date);
    }

    public static function nextWeek($date = null, $addweeks = 1)
    {
        if($date == null){
            return Carbon::now()->startOfWeek(Carbon::SATURDAY)->addWeeks($addweeks);
        }

        return Carbon::parse($date)->startOfWeek(Carbon::SATURDAY)->addWeeks($addweeks);
    }

    public static function prevWeek($date = null, $prevAvailable = false)
    {
        if($date == null && $prevAvailable){
            return Carbon::now()->startOfWeek(Carbon::SATURDAY)->subWeek();
        } elseif (!$prevAvailable && ($date == null || Carbon::parse($date)->subWeek()->lessThanOrEqualTo(Carbon::now()->startOfWeek(Carbon::SATURDAY)))) {
            return null;
        }

        return Carbon::parse($date)->startOfWeek(Carbon::SATURDAY)->subWeek();
    }

    public static function startOfWeek($date = null, $format = 'Y-m-d')
    {
        if($date == null){
            return Carbon::now()->startOfWeek(Carbon::SATURDAY);
        }

        return Carbon::parse($date)->startOfWeek(Carbon::SATURDAY);
    }

    public static function endOfWeek($date = null, $format = 'Y-m-d')
    {
        if($date == null){
            return Carbon::now()->endOfWeek(Carbon::Friday);
        }

        return Carbon::parse($date)->endOfWeek(Carbon::SATURDAY);
    }

    public static function parser($date)
    {
        return Carbon::parse($date);
    }



}
