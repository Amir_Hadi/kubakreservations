<?php

namespace App;

use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'name', 'email', 'password',
//    ];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function role()
    {
        return $this->belongsTo(\App\Role::class, 'role_id');
    }

    public function reserves()
    {
        return $this->hasMany(Reserve::class, 'user_id');
    }

    public function reserve($schedule)
    {
        if ($reserve = $this->reserves()->where([
            ['user_id', '=', $this->id],
            ['schedule_id', '=', $schedule->id],
        ])->first()) {
            return $reserve;
        }
        return false;
    }

    public function access($table)
    {
        return $this->role->access()->where('table_name', $table)->get();
    }

    public function reserveAvailable(ScheduleFood $schedule)
    {
        $scheduledate = Carbon::parse($schedule->date)->startOfWeek(Carbon::SATURDAY);
        $thisweek = Carbon::now()->startOfWeek(Carbon::SATURDAY);
        $thisweek1 = Carbon::now()->startOfWeek(Carbon::SATURDAY);

        if ($scheduledate->greaterThanOrEqualTo($thisweek->addWeeks(2))){
            return true;
        }elseif ($scheduledate->greaterThanOrEqualTo($thisweek1->addWeek())){
            return !Carbon::now()->startOfWeek(Carbon::SATURDAY)->addDays(AdminConfig::getDay()-1)
                ->addHours(AdminConfig::getHour())->addMinutes(AdminConfig::getMinute())->isPast();
        }else {
            return false;
        }

    }

    public function cancelReserveAvailable(ScheduleFood $schedule)
    {
        $dayOfWeek = $schedule->date->startOfWeek(Carbon::SATURDAY)->dayOfWeek;

        $scheduledate = Carbon::parse($schedule->date)->startOfWeek(Carbon::SATURDAY);
        $thisweek = Carbon::now()->startOfWeek(Carbon::SATURDAY);

        if ($scheduledate->greaterThanOrEqualTo($thisweek->addWeeks(2))){
            return true;
        }elseif ($scheduledate->equalTo($thisweek->addWeek())){
            if (
                Carbon::now()->dayOfWeek == AdminConfig::getDay() &&
                Carbon::parse(Carbon::now()->format('H:i'))->greaterThan(Carbon::parse(AdminConfig::getTime()))
            ){
                return false;
            }else{
                return true;
            }

        }else {
            return false;
        }

    }


    public function reservationsByDate($date)
    {
        return $this->reserves()->join('schedule_foods', 'schedule_id', 'schedule_foods.id')
            ->where('date', '>=', $date)
            ->where('date', '<', $date)
            ->get();
    }

    public static function usersReservedSchedule($date = null)
    {
        $users = ScheduleFood::weeklySchedules($date)->orderBy('date')
            ->join('reserves', 'schedule_foods.id', 'reserves.schedule_id')
            ->join('users', 'reserves.user_id', 'users.id');

        $userids = array_column($users->select('users.id')->distinct()->get()->toArray(), 'id');

        return User::whereIn('id', $userids)->orderBy('lastname')->get();
    }

    public function logs()
    {
        return $this->hasMany(Log::class, 'user_id');
    }

    public function fullName()
    {
        return " " . $this->firstname . " " . $this->lastname . " ";
    }

}
