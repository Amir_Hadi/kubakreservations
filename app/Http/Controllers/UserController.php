<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Http\Requests\UserRequest;
use App\Log;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $this->authorizeForUser(auth()->user(), 'viewAny', \App\User::class);
        $users = User::orderBy('employnumber')->orderBy('lastname')->paginate(10);
        return view('users.index', compact('users'));
    }

    public function create()
    {
        $this->authorizeForUser(auth()->user(), 'create', \App\User::class);
        $roles = Role::userRolesToProve(auth()->user());
        return view('users.create', compact('roles'));
    }

    public function store(UserRequest $request)
    {

        $this->authorizeForUser(auth()->user(), 'create', \App\User::class);

        $user = User::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'employnumber' => $request->employnumber,
            'phone' => $request->phone,
            'role_id' => $request->role_id,
            'password' => bcrypt($request->password)
        ]);

        Log::log('users', "ایجاد",' ایجاد کاربر جدید توسط '. auth()->user()->firstname . " " . auth()->user()->lastname);


        session()->flash('success', 'کاربر با موفقیت افزوده شد');

        return redirect(route('users.index'));
    }

    public function edit(User $user)
    {
        $this->authorizeForUser(auth()->user(), 'update', $user);

        $roles = Role::all();
        return view('users.edit', compact('roles', 'user'));
    }

    public function show(User $user)
    {
        $this->authorizeForUser(auth()->user(), 'view', $user);
        $reserves = $user->reserves()->join('schedule_foods', 'reserves.schedule_id', '=', 'schedule_foods.id');
        if(request('startdate') && request('enddate')){
            $startdate = Helper::toGregorian(Carbon::parse(request('startdate')));
            $endtdate = Helper::toGregorian(Carbon::parse(request('enddate')));
            $reserves = $reserves->where('date', '>=', $startdate)
            ->where('date', '<', $endtdate->addDay());
        }
        $reserves = $reserves->orderBy('date')->get();
        return view('users.show', compact('user', 'reserves'));
    }

    public function update(User $user, Request $request)
    {
//        dd($request->all());
        $this->authorizeForUser(auth()->user(), 'update', $user);

        $this->validate($request,[
            'firstname' => ['required', 'min:3'],
            'lastname' => ['required', 'min:3'],
            'employnumber' => 'required',
            'phone' => 'required',
            'role_id' => 'required'
        ],[
            'firstname.required' => 'نام کاربر الزامی‌ است',
            'firstname.min' => 'نام کاربر حداقل باید ٣ کاراکتر باشد',
            'lastname.required' => 'نام خانوادگی کاربر الزامی‌ است',
            'lastname.min' => 'نام خانوادگی کاربر حداقل باید ٣ کاراکتر باشد',
            'employnumber.required' => 'شماره کارمندی الزامی‌ است',
            'phone.required' => 'شماره موبایل الزامی‌ است',
            'role.required' => 'انتخاب نقش الزامی‌ است'
        ]);

        $user->update($request->all());


        Log::log('users',  "ویرایش",' به روز آوری اطلاعات کاربر ' . $user->fullName() . ' توسط ' . auth()->user()->fullName() . ' به شماره کارمندی '.  auth()->user()->employnumber);


        session()->flash('success', 'اطلاعات کاربر با موفقیت ویرایش شد');

        return redirect('/users');
    }

    public function passwordEdit(User $user)
    {
        $this->authorizeForUser(auth()->user(), 'update', $user);
        return view('users.passwordedit', compact('user'));
    }


    public function passwordUpdate(Request $request, User $user)
    {
        $this->authorizeForUser(auth()->user(), 'update', $user);

        $this->validate($request, [
            'password' => 'required|min:4|confirmed'
        ], [
            'password.required' => 'رمز عبور نمی‌تواند خالی باشد',
            'password.min' => 'رمز عبور نمی‌تواند کمتر از جهار کاراکتر باشد',
            'password.confirmed' => 'فیلدها با هم تطابق ندارند',
        ]);
        $user->update(['password' => bcrypt($request->password)]);

        Log::log('users',  "ویرایش رمز",'رمز کاربر' . $user->fullName() . ' توسط '.  auth()->user()->fullname()  .' با موفقیت به روز شد');


        session()->flash('success', 'رمز کاربر توسط '.  auth()->user()->fullName() .' با موفقیت به روز شد');

        return redirect('/users');
    }

    public function destroy(User $user)
    {
        $usr = $user;
        $this->authorizeForUser(auth()->user(), 'delete', $user);
        $user->reserves()->delete();
        $user->delete();

        Log::log('users', "حذف","حذف کاربر {$usr->fullName()}" . 'توسط' . auth()->user()->fullName() . " ( " . auth()->user()->employnumber . " ) " );


        session()->flash('delete', 'کاربر با موفقیت حذف شد');

        return redirect('/users');
    }

    public function editOwnPassword(User $user)
    {
        $this->authorizeForUser(auth()->user(), 'updatePassword', $user);

        return view('users.ownpasswordedit', compact('user'));

    }

    public function updateOwnPassword(Request $request, User $user)
    {

        $this->authorizeForUser(auth()->user(), 'updatePassword', $user);

        $this->validate($request, [
            'currentpassword' => 'required',
            'password' => 'required|confirmed'
        ],[
            'currentpassword.required' => 'وارد کردن رمز عبور فعلی الزامی است',
            'password.required' => 'وارد کردن فیلد رمز عبور الزامی‌ است',
            'password.confirmed' => 'فیلدهای وارد شده با هم تطابق ندارند'
        ]);


        if(auth()->attempt(['employnumber' => $user->employnumber, 'password' => $request->currentpassword])){

            if (!auth()->attempt(['employnumber' => $user->employnumber, 'password' => $request->password])){
                $user->update([
                    'password' => bcrypt($request->password)
                ]);

                Log::log('users', "ویرایش رمز  {$user->fullName()}", "رمز کاربر  {$user->fullName()}  توسط " . auth()->user()->fullName() . " ( " . auth()->user()->employnumber . " ) " . "تغغیر یافت");

                session()->flash('success', 'رمز شما با موفقیت به روز شد');

                return redirect('/home');
            } else {
                return back()->withErrors(['message' => 'رمز جدید نباید با رمز قبلی یکسان باشد']);
            }
        }else {
            return back()->withErrors(['message' => 'رمز قبلی صحیح وارد نشده است']);
        }
    }
}
