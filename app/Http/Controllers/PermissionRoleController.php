<?php

namespace App\Http\Controllers;

use App\AdminConfig;
use App\Food;
use App\Log;
use App\Permission;
use App\PermissionRole;
use App\Role;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermissionRoleController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function create(Role $role)
    {
        $this->authorizeForUser(auth()->user(), 'create', \App\PermissionRole::class);
        $users = Permission::where('title_english', 'like','%-user')->get();
        $pass = Permission::where('title_english', 'like','edit-pass')->get();
        $foods = Permission::where('title_english', 'like','%-food')->get();
        $roles = Permission::where('title_english', 'like','%-role')->get();
        $schedules = Permission::where('title_english', 'like','%-schedule')->get();
        $configs = Permission::where('title_english', 'like','%-config')->get();
        $reserves = Permission::where('title_english', 'like','%-reserve')->get();
        $permissions = Permission::where('title_english', 'like','%-permissionrole')->get();

        $permissionroles =array_column(PermissionRole::select('permission_id')->where('role_id', $role->id)->get()->toArray(), 'permission_id');
//        dd($permissionroles);
        return view('permissionrole.create', compact('role', 'users', 'foods', 'roles', 'schedules', 'configs', 'reserves', 'permissions', 'pass', 'permissionroles'));
    }

    public function store(Request $request, Role $role)
    {
        $this->authorizeForUser(auth()->user(), 'create', \App\PermissionRole::class);

        $permissionroles =array_column(PermissionRole::select('permission_id')->where('role_id', $role->id)->get()->toArray(), 'permission_id');
        $notcheckedpermissions = array_values(array_diff($permissionroles, array_keys($request->all())));

        foreach($request->all() as $key => $value){
            if(in_array($key, $permissionroles)){
                continue;
            }elseif(! in_array($key, $permissionroles)){
                PermissionRole::create([
                    'permission_id' => $key,
                    'role_id' => $role->id,
                ]);
            }
        }

        DB::table('permission_roles')->where('role_id', $role->id)->whereIn('permission_id', $notcheckedpermissions)->delete();

        Log::log('permission_roles', 'ایجاد' , 'دسترسی جدید توسط ' . auth()->user()->fullName() . " به شماره کارمندی " .  auth()->user()->employnumber  . ' با موفقیت افزوده شد.');

        session()->flash('success', 'دسترسی جدید با موفقیت افزوده شد.');

        return redirect('/roles');
    }

    public function destroy(PermissionRole $permissionrole)
    {
        $this->authorizeForUser(auth()->user(), 'delete', $permissionrole);

        $permissionrole->delete();

        Log::log('permi ssion_roles', "حذف", 'دسترسی جدید توسط ' . auth()->user()->fullName() . " به شماره کارمندی " .  auth()->user()->employnumber  . ' با موفقیت افزوده شد.');


        session()->flash('info', 'با موفقیت حذف شد');

        return redirect('/roles');
    }
}
