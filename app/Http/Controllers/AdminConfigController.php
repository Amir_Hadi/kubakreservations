<?php

namespace App\Http\Controllers;

use App\AdminConfig;
use App\Day;
use App\Log;
use Illuminate\Http\Request;

class AdminConfigController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorizeForUser(auth()->user(), 'viewAny', AdminConfig::class);

        $configs = AdminConfig::all();


        return view('configs.index', compact('configs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorizeForUser(auth()->user(), 'create', AdminConfig::class);
        return view('configs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->authorizeForUser(auth()->user(), 'create', AdminConfig::class);

        $this->validate($request, [
            'day' => 'required',
            'time' => 'required'
        ],[
            'day.required' => 'انتخاب روز الزامی‌است',
            'time.required' => 'وارد کردن زمان الزامی‌است'
        ]);

        AdminConfig::create([
            'day' => $request->day,
            'time' => $request->time
        ]);

        Log::log('adminconfigs', 'ایجاد', " پیکر بندی جدید توسط کاربر " . auth()->user()->fullName() . " ( " . auth()->user()->employnumber . " ) " . " ایجاد شد. ");


        session()->flash('success', 'پیکربندی جدید با موفقیت افزوده شد.');

        return redirect(route('configs.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminConfig  $adminConfig
     * @return \Illuminate\Http\Response
     */
    public function show(AdminConfig $adminConfig)
    {
        $this->authorizeForUser(auth()->user(), 'view', $adminConfig);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminConfig  $adminConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminConfig $config)
    {
//        dd($adminConfig);
        $this->authorizeForUser(auth()->user(), 'update', $config);
        return view('configs.edit', compact('config'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminConfig  $adminConfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminConfig $config)
    {
        $this->authorizeForUser(auth()->user(), 'update', $config);

        $this->validate($request, [
            'day' => 'required',
            'time' => 'required'
        ],[
            'day.required' => 'انتخاب روز الزامی‌است',
            'time.required' => 'وارد کردن زمان الزامی‌است'
        ]);

        $config->update($request->all());

        Log::log('adminconfigs', 'ویرایش', "پیکر بندی " . $config->id . " توسط کاربر" . auth()->user()->name . " ( " . auth()->user()->employnumber . " ) " . "ویرایش شد");


        session()->flash('success', 'پیکر بندی با موفقیت تغییر یافت');

        return redirect(route('configs.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminConfig  $adminConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminConfig $config)
    {
        $this->authorizeForUser(auth()->user(), 'delete', $config);


        $config->delete();

        Log::log('adminconfigs', 'حذف', " پیکر بندی توسط کاربر ". auth()->user()->name . " ( " . auth()->user()->employnumber . " ) " . " حذف شد. ");

        session()->flash('delete', 'پیکر بندی با موفقیت حذف شد.');

        return redirect(route('configs.index'));
    }
}
