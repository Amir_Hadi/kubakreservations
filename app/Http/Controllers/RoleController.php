<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use App\Log;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorizeForUser(auth()->user(), 'viewany', \App\Role::class);
        $roles = Role::paginate(10);
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorizeForUser(auth()->user(), 'create', \App\Role::class);

        $roles = Role::userRolesToProve(auth()->user());


        return view('roles.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {

        $rolesToProveString = '';

        $rolesToProve = array_values($request->all('rolesToProve'))[0];

        if(!empty($rolesToProve)){
            foreach($rolesToProve as $roleToProve){
                $rolesToProveString = $rolesToProveString . "{$roleToProve}, ";
            }
        }


        $this->authorizeForUser(auth()->user(), 'create', \App\Role::class);

        $role = Role::create([
           'title' => request('title'),
            'rolesToProve' => $rolesToProveString,
        ]);

        Log::log('roles', 'ایجاد', " نقش جدید توسط کاربر " . auth()->user()->fullName()  . " ( " . auth()->user()->employnumber . " ) " . " ایجاد شد. ");


        session()->flash('success', 'نقش جدید با موفقیت اضافه شد');

        return redirect(route('roles.index'));
    }

    public function show(Role $role)
    {
        $this->authorizeForUser(auth()->user(), 'view', $role);
        return view('roles.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $this->authorizeForUser(auth()->user(), 'update', $role);

        $userToProveRoles = Role::userRolesToProve(auth()->user());
        $roleToProveRoles = array_column(Role::roleToProveRoles($role)->toArray(), 'id');

        return view('roles.edit', compact('role','userToProveRoles', 'roleToProveRoles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, Role $role)
    {

        $rl = $role;

        $rolesToProveString = '';

        $rolesToProve = array_values($request->all('rolesToProve'))[0];

        if(!empty($rolesToProve)){
            foreach($rolesToProve as $roleToProve){
                $rolesToProveString = $rolesToProveString . "{$roleToProve}, ";
            }
        }

        $this->authorizeForUser(auth()->user(), 'update', $role);

        $role->update([
            'title' => $request->title,
            'rolesToProve' => $rolesToProveString
        ]);

        Log::log('roles', " ویرایش " , "ویرایش {$rl->title} به {$role->title} توسط کاربر  " .  auth()->user()->fullName()  . " ( " . auth()->user()->employnumber . " ) " );

        session()->flash("success", "با موفقیت ویرایش شد");

        return redirect("/roles");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $rl = $role;

        $this->authorizeForUser(auth()->user(), 'delete', $role);

        $role->delete();

        Log::log('roles', "حذف", "حذف {$rl->title} توسط کاربر " .  auth()->user()->fullName()  . " ( " . auth()->user()->employnumber . " ) " );


        session()->flash("delete", "نقش مورد نظر با موفقیت حذف شد");

        return redirect('roles');
    }
}
