<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Log;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorizeForUser(auth()->user(), 'viewAny', Log::class);

        $logs = Log::select('*')->orderBy('logs.id', 'desc')->join('users', 'users.id', 'logs.user_id');

        if (request('employnumber')){
            $logs = $logs->where('employnumber', request('employnumber'));
        }
        if (request('startdate')){
            $date = Helper::toGregorian(Carbon::parse(request('startdate')));
            $logs = $logs->where('date', '>=', $date);

        }
        if (request('enddate')){
            $date = Carbon::parse(Helper::toGregorian(Carbon::parse(request('enddate'))))->addDay()->format('Y-m-d');
            $logs = $logs->where('date', '<', $date);
        }

        $logs = $logs->paginate(20);

        return view('logs.index', compact('logs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Log  $log
     * @return \Illuminate\Http\Response
     */
    public function show(Log $log)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Log  $log
     * @return \Illuminate\Http\Response
     */
    public function edit(Log $log)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Log  $log
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Log $log)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Log  $log
     * @return \Illuminate\Http\Response
     */
    public function destroy(Log $log)
    {
        //
    }
}
