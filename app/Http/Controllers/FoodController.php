<?php

namespace App\Http\Controllers;

use App\Food;
use App\Http\Requests\FoodRequest;
use App\Http\Requests\FoodPolicy;
use App\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        $this->authorizeForUser(auth()->user(), 'viewAny', \App\Food::class);
        $foods = Food::paginate(10);
        return view('foods.index', compact('foods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', [\App\Food::class, auth()->user()]);
        return view('foods.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FoodRequest $request)
    {

        $this->authorize('create', [\App\Food::class, auth()->user()]);

        $imageurl = "";
        if(request('image')){
            $imageurl = str_replace('public', '/storage',Storage::putFile('/public/posts',request('image')));
        }

        $food = Food::create([
            'name' => request('name'),
            'description' => request('description'),
            'cost' => request('cost'),
            'image' => $imageurl
        ]);

        Log::log('foods', "ایجاد", $food->name  . " توسط کاربر " . auth()->user()->fullName() . " ( " . auth()->user()->employnumber . " ) " . " ایجاد شد. ");


        session()->flash("success", "{$food->name} با موفقیت اضافه شد ");

        return redirect("/foods");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Food  $food
     * @return \Illuminate\Http\Response
     */
    public function show(Food $food)
    {
        $this->authorizeForUser(auth()->user(), 'view', $food);
        return view('foods.show', compact('food'));
    }


    public function edit(Food $food)
    {
        $this->authorizeForUser(auth()->user(), 'update', $food);
        return view('foods.edit', compact('food'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Food  $food
     * @return \Illuminate\Http\Response
     */
    public function update(FoodRequest $request, Food $food)
    {
        $fd = $food;
        $this->authorizeForUser(auth()->user(), 'update', $food);
        $food->update($request->all());

        Log::log('foods', "ویرایش ", " غذای " . $food->name . " توسط کاربر " . auth()->user()->fullName() . " ( " . auth()->user()->employnumber . " ) " . " تغییر یافت ");


        session()->flash("success", "{$food->name} با موفقیت ویرایش شد ");
        return redirect("/foods");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Food  $food
     * @return \Illuminate\Http\Response
     */
    public function destroy(Food $food)
    {
        $fd = $food;
        $this->authorizeForUser(auth()->user(), 'delete', $food);
        if(!empty($food->schedules()) && $food->schedules->count() > 0){
            session()->flash('danger', 'امکان حذف غذا وجود ندارد');
            return back();
        } else {
            $food->delete();

            Log::log('foods', "حذف", " غذای " . $fd->name . " توسط کاربر " . auth()->user()->fullName() . " ( " . auth()->user()->employnumber . " ) " . " حذف شد. ");

            session()->flash('delete', "با موفقیت حذف شد");
        }
        return redirect("/foods");
    }
}
