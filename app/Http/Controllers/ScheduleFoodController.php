<?php

namespace App\Http\Controllers;

use App\Food;
use App\Helper\Helper;
use App\Http\Requests\ScheduleFoodRequest;
use App\Log;
use App\Reserve;
use App\ScheduleFood;
use App\User;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class ScheduleFoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorizeForUser(auth()->user(), 'viewAny', \App\ScheduleFood::class);
        $schedules = ScheduleFood::select('*')->orderBy('date')->paginate(10);
        return view('schedules.index', compact('schedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorizeForUser(auth()->user(), 'create', \App\ScheduleFood::class);

        $foods = Food::all();
        $prevweek = null;
        $addweeks = null;
        if(request('date') && Carbon::parse(request('date'))->greaterThan(Carbon::now()->addWeek())){
            $addweeks = 1;
        }else{
            $addweeks = 2;
        }
        $nextweek = \App\Helper\Helper::nextWeek(request('date'), $addweeks);
        if(
            ScheduleFood::weeklySchedules(Helper::prevWeek(request('date'), true))->get()->count() > 0
            || Carbon::parse(request('date'))->greaterThan(Carbon::now()->startOfWeek(Carbon::SATURDAY)->addWeek())
        ){
            $prevweek = Helper::prevWeek(request('date'), true);
        }

        return view('schedules.create', compact('foods', 'nextweek', 'prevweek'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorizeForUser(auth()->user(), 'create', \App\ScheduleFood::class);

        $schedules = $request->only('schedules')['schedules'];

        if(!empty($schedules)){
            foreach ($schedules as $key => $value){
                if(ScheduleFood::where('date', Carbon::parse($key))->first()){
                    $plan = ScheduleFood::where('date', Carbon::parse($key))->get()->first();
                    if ($plan->food_id != $value){
                        $plan->update([
                            'food_id' => $value
                        ]);
                        if($value != -1){
                            Log::log('schedule_foods', 'به روز آوری', " غذای روز " . $plan->dayName() . " " . "<span dir=\"rtl\">" . $plan->showDateInJalali() . "</span>" . " توسط کاربر " . auth()->user()->fullName() . " ( " .  auth()->user()->employnumber . " ) "  . "تغییر یافت");
                        }else{
                            $plan->reserves()->delete();
                            Log::log('schedules', 'حذف', "تمام رزرهای مربوط به روز" . $plan->dayName() . " " . "<span dir=\"rtl\">" . $plan->showDateInJalali() . "</span>" . " توسط کاربر " . auth()->user()->fullName() . " ( " .  auth()->user()->employnumber . " ) " .  " به علت انتخاب تعطیلی حذف شد " );
                        }
                    }
                } else {

                    $schedule = ScheduleFood::create([
                        'date' => Carbon::parse($key),
                        'food_id' => $value
                    ]);

                    Log::log('schedule_foods', 'ایجاد' , "برنامه جدید برای روز" . $schedule->dayName() . " " . "<span dir=\"rtl\">" . $schedule->showDateInJalali() . "</span>" ." توسط کاربر " . auth()->user()->fullName() . " ( " .  auth()->user()->employnumber . " ) " .  " ایجاد شد " );
                }

            }
            session()->flash('success', 'برنامه با موفقیت اضافه شد');
        }

        return redirect('/schedules/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ScheduleFood  $scheduleFood
     * @return \Illuminate\Http\Response
     */
    public function show(ScheduleFood $scheduleFood)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ScheduleFood  $scheduleFood
     * @return \Illuminate\Http\Response
     */
    public function edit(ScheduleFood $schedule)
    {
        $this->authorizeForUser(auth()->user(), 'update', $schedule);

        $foods = Food::all();
        return view('schedules.edit', compact('schedule', 'foods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ScheduleFood  $scheduleFood
     * @return \Illuminate\Http\Response
     */
    public function update(ScheduleFoodRequest $request, ScheduleFood $schedule)
    {
        $this->authorizeForUser(auth()->user(), 'update', $schedule);

        $schedule->update([
            'food_id'=> $request->food_id
        ]);

        Log::log('schedule_foods', 'به روز آوری', " غذای روز " . $schedule->dayName() . " " . $schedule->showDateInJalali() . " توسط کاربر " . auth()->user()->fullName() . " ( " .  auth()->user()->employnumber . " ) "  . "تغییر یافت");


        return redirect(route('schedules.index'));
    }


    public function showSchedulesReservedByUsers()
    {
        $date = null;
        $nextweek = null;
        $prevweek = null;
        if(request('date')){
            $date = Carbon::parse(request('date'));
        } else {
            $date = Carbon::now()->addWeek();
        }

        if(ScheduleFood::weeklySchedules(Helper::nextWeek($date))->get()->count() > 0){
            $nextweek = Helper::nextWeek($date);
        }
        if(ScheduleFood::weeklySchedules(Helper::prevWeek($date, true))->get()->count() > 0){
            $prevweek = Helper::prevWeek($date, true);
        }
        $schedules = ScheduleFood::weeklySchedules($date)->orderBy('date')->get();

        $users = User::usersReservedSchedule($date);


        return view('schedules.show', compact('schedules', 'users', 'nextweek', 'prevweek'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ScheduleFood  $scheduleFood
     * @return \Illuminate\Http\Response
     */
    public function destroy(ScheduleFood $scheduleFood)
    {
        $this->authorizeForUser(auth()->user(), 'delete', $scheduleFood);
        $scheduleFood->delete();
        Log::log('schedules', 'حذف', "برنامه روز" . $scheduleFood->dayName() . " " . "<span dir=\"rtl\">" . $scheduleFood->schedule->showDateInJalali() . "</span>" . " توسط کاربر " . auth()->user()->name . " ( " .  auth()->user()->employnumber . " ) " .  " حذف شد " );

        return redirect("/schedules");

    }


    public function deleteReservations(ScheduleFood $scheduleFood)
    {
        $this->authorizeForUser(auth()->user(), 'delete', $scheduleFood);

        DB::table('reserves')->where('schedule_id', $scheduleFood->id)->delete();

        Log::log('reserves', 'حذف همه‌ی رزروها', "همه ی رزروهای مربوط به تاریخ " . $scheduleFood->dayName() . " " . "<span dir=\"rtl\">" .$scheduleFood->showDateInJalali() . "</span>" . " توسط کاربر " . auth()->user()->name . " ( " .  auth()->user()->employnumber . " ) " .  " حذف شد " );


        session()->flash('delete', " همه‌ی رزروهای روز {$scheduleFood->dayName()} باموفقیت حذف شد  ");

        return redirect('/schedules/shownextweek');
    }


    public function print()
    {
        $date = null;
        if(request('date')){
            $date = Carbon::parse(request('date'));
        } else {
            $date = Carbon::now()->addWeek();
        }

        $schedules = ScheduleFood::weeklySchedules($date)->orderBy('date')->get();

        $users = User::usersReservedSchedule($date);

        return view('schedules.print', compact('schedules', 'users'));
    }


    public function deleteUserReserve(ScheduleFood $schedule, User $user)
    {

        DB::table('reserves')->where('schedule_id', $schedule->id)
            ->where('user_id', $user->id)->delete();

        Log::log('reserves', 'حذف', "رزرو مربوط به کاربر" . $user->name . " ( " . $user->employnumber . " ) " . " به تاریخ ". "<span dir=\"rtl\">" .$schedule->showDateInJalali() . "</span>" . " با غذای " .  $schedule->food->name . " توسط کاربر " . auth()->user()->name . " ( " .  auth()->user()->employnumber . " ) "  . " حذف شد ");

        session()->flash('delete', 'رزرو کاربر'. "{$user->name}" . 'با موفقیت لغو شد');

        return redirect('/schedules/shownextweek');
    }
}
