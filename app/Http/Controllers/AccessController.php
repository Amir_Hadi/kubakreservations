<?php

namespace App\Http\Controllers;

use App\Access;
use App\Helper\Helper;
use App\Http\Requests\AccessRequest;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorizeForUser(auth()->user(), 'viewAny', \App\Access::class);
        $accesses = Access::all();
        return view('accesses.index', compact('accesses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorizeForUser(auth()->user(), 'create', \App\Access::class);
        $roles = Role::all();
        $tables = Helper::dbTables();
        return view('accesses.create', compact('roles', 'tables'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccessRequest $request)
    {
        $this->authorizeForUser(auth()->user(), 'create', \App\Access::class);

        $access = Access::create([
            'role_id' => $request->role_id,
            'table_name' => $request->table_name,
            'create' => (boolean)$request->create,
            'read' => (boolean)$request->read,
            'update' => (boolean)$request->update,
            'delete' => (boolean)$request->delete
        ]);

        session()->flash('success', 'دسترسی جدید با موفقیت ایجاد شد');

        return redirect(route('accesses.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Access  $access
     * @return \Illuminate\Http\Response
     */
    public function show(Access $access)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Access  $access
     * @return \Illuminate\Http\Response
     */
    public function edit(Access $access)
    {
        $this->authorizeForUser(auth()->user(), 'update', $access);
        $roles = Role::all();
        $tables = Helper::dbTables();
        return view('accesses.edit', compact('access', 'roles', 'tables'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Access  $access
     * @return \Illuminate\Http\Response
     */
    public function update(AccessRequest $request, Access $access)
    {
        $this->authorizeForUser(auth()->user(), 'update', $access);

        $access->update([
            'role_id' => $request->role_id,
            'table_name' => $request->table_name,
            'create' => (boolean)$request->create,
            'read' => (boolean)$request->read,
            'update' => (boolean)$request->update,
            'delete' => (boolean)$request->delete,
        ]);
        session()->flash('success', 'دسترسی با موفقیت ویرایش شد.');
        return redirect(route('accesses.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Access  $access
     * @return \Illuminate\Http\Response
     */
    public function destroy(Access $access)
    {
        $this->authorizeForUser(auth()->user(), 'delete', $access);
        $access->delete();

        return redirect(route('accesses.index'));

    }
}
