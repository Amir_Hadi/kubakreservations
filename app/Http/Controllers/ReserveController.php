<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Log;
use App\Reserve;
use App\ScheduleFood;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;

class ReserveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {

        $reserves = Reserve::latest('reserves.created_at')->where('user_id', auth()->id())
            ->join('schedule_foods', 'reserves.schedule_id', 'schedule_foods.id')
            ->orderBy('date')->paginate(5);
//        dd($reserves);
        return view('reserves.index', compact('reserves'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorizeForUser(auth()->user(), 'create', \App\Reserve::class);
        $prevweek = null;
        $nextweek = null;
        $carbon = new Carbon();
        $schedules = null;
        if(request('date')){
            $schedules = ScheduleFood::weeklySchedules(request('date'));
            $prevweek = ScheduleFood::ifHasPreviousReservations(request('date'));
            $nextweek = ScheduleFood::ifNextWeekReservationAvailable(request('date'));
        }else{
            $prevweek = ScheduleFood::ifHasPreviousReservations(Carbon::now()->startOfWeek(Carbon::SATURDAY)->addWeek()->format('Y-m-d'));
            $nextweek = ScheduleFood::ifNextWeekReservationAvailable(Carbon::now()->startOfWeek(Carbon::SATURDAY)->addWeek()->format('Y-m-d'));
            $schedules = ScheduleFood::weeklySchedules($carbon->now()->startOfWeek(Carbon::SATURDAY)->addWeek()->format('Y-m-d'));
        }


        $schedules = $schedules->get();
        return view('reserves.create', compact('schedules', 'prevweek', 'nextweek'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $reservesAvailable = array_column(ScheduleFood::weeklySchedules(Helper::nextWeek())->select('id')->get()->toArray(), 'id'));
//        dd($request);
        $this->authorizeForUser(auth()->user(), 'create', \App\Reserve::class);
        $requestedSchedules = array_keys($request->all()['reserves']);
//        dd($requestedSchedules);
        $reservedSchedules = Reserve::weeklyReserves(auth()->user(), request('date'));
        $canceledSchedules = array_diff($reservedSchedules, $requestedSchedules);
        foreach($requestedSchedules as $schedule){
            if(! Reserve::exists(auth()->user(), $schedule) && auth()->user()->reserveAvailable(ScheduleFood::find($schedule))) {

                Reserve::create([
                    'schedule_id' => $schedule,
                    'user_id' => auth()->id()
                ]);

                $sche = ScheduleFood::where('id', $schedule)->get()->first();
                Log::log('reserve', "ایجاد"," رزرو غذای روز " . $sche->dayName() . " به تاریخ " . "<span dir=\"rtl\">" . $sche->showDateInJalali() . "</span>" . " توسط کاربر " . auth()->user()->fullName() . " ( " .  auth()->user()->employnumber . " ) " );

                session()->flash('success', 'رزرو انجام شد.');
            }
        }
        foreach ($canceledSchedules as $schedule){
            $sche = ScheduleFood::where('id', $schedule)->get()->first();
            Reserve::where('user_id', auth()->id())->where('schedule_id', $schedule)->delete();
            Log::log('reserve', "حذف"," لغو رزرو غذای روز " . $sche->dayName() . " به تاریخ " . "<span dir=\"rtl\">" . $sche->showDateInJalali() . "</span>" . " توسط کاربر " . auth()->user()->fullName() . " ( " .  auth()->user()->employnumber . " ) " );

        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reserve  $reseve
     * @return \Illuminate\Http\Response
     */
    public function show(Reserve $reseve)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reserve  $reseve
     * @return \Illuminate\Http\Response
     */
    public function edit(Reserve $reseve)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reserve  $reseve
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reserve $reseve)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reserve  $reseve
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reserve $reserve)
    {
        $this->authorizeForUser(auth()->user(), 'delete', $reserve);
        if($reserve != null){
            $reserve->delete();

            Log::log('reserve', "حذف"," لغو رزرو غذای روز " . $reserve->schedule->dayName() . " به تاریخ " . "<span dir=\"rtl\">" . $reserve->schedule->showDateInJalali() . "</span>" . " توسط کاربر " . auth()->user()->fullName() . " ( " .  auth()->user()->employnumber . " ) " );



            session()->flash('delete', 'رزرو شما با موفقیت لغو شد');
        }
        return redirect('/reserves/create');
    }
}
