<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_id' => 'required',
            'firstname' => 'required|min:3',
            'lastname' => 'required|min:3',
            'employnumber' => 'required|unique:users',
            'phone' => 'required|min:11|max:11',
            'password' => 'required|min:6|confirmed'
        ];
    }

    public function messages()
    {
        return [
            'role_id.required' => 'انتخاب نقش الزامی‌ است',
            'firstname.required' => ' نام کاربر الزامی‌ است',
            'firstname.min' => 'نام کاربر حداقل باید ٣ کاراکتر باشد',
            'lastname.required' => ' نام خانوادگی کاربر الزامی‌ است',
            'lastname.min' => 'نام خانوادگی کاربر حداقل باید ٣ کاراکتر باشد',
            'employnumber.required' => 'شماره کارمندی الزامی است',
            'employnumber.unique' => 'شماره کارمندی وارد شده از قبل موجود است',
            'phone.required' => 'شماره موبایل الزامی است',
            'phone.min' => 'شماره موبایل باید ١١ رقم باشد',
            'phone.max' => 'شماره موبایل باید ١١ رقم باشد',
            'password.required' => 'رمز عبور الزامی است',
            'password.min' => 'رمز عبور حداقل باید ٦ رقم باشد',
            'password.confirmed' => 'فیلدهای رمز عبور و تکرار رمزعبور با هم تطابق ندارند',
        ];
    }
}
