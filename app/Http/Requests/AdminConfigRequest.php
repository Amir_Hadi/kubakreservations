<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'value' => 'required|string'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'وارد کردن نام ضروری است',
            'name.string' => 'فیلد نام باید از نوع رشته باشد',
            'value.required' => 'وارد کردن مقدار ضروری است',
            'value.string' => 'فیلد مقدار باید از نوع رشته باشد',
        ];
    }
}
