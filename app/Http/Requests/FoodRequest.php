<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FoodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4',
            'description' => 'required|min:4',
            'image' => 'mimes:jpg,jpeg,png',
            'cost' => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'وارد کردن عنوان غذا الزامی‌ است',
            'name.min' => 'حداقل تعداد کاراکتر ٤ است',
            'description.required' => 'وارد کردن توضیحات غذا الزامی‌ است',
            'description.min' => 'حداقل تعداد کاراکتر ٤ است',
            'image.mimes' => 'تصویر باید به یکی از فرمت‌های jpg, jpeg, png باشد',
            'cost.required' => 'وارد کردن قیمت ضروری است',
            'cost.integer' => 'قیمت باید حتما به ؟ صورت عددی و بدون ممیز باشد.'
        ];
    }
}
