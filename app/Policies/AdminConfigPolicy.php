<?php

namespace App\Policies;

use App\User;
use App\AdminConfig;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminConfigPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any admin configs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role->permissions()->where('title_english', 'read-config')->get()->first();
    }

    /**
     * Determine whether the user can view the admin config.
     *
     * @param  \App\User  $user
     * @param  \App\AdminConfig  $adminConfig
     * @return mixed
     */
    public function view(User $user, AdminConfig $adminConfig)
    {
        return $user->role->permissions()->where('title_english', 'read-config')->get()->first() && $adminConfig;
    }

    /**
     * Determine whether the user can create admin configs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role->permissions()->where('title_english', 'add-config')->get()->first() && AdminConfig::all()->count() == 0;
    }

    /**
     * Determine whether the user can update the admin config.
     *
     * @param  \App\User  $user
     * @param  \App\AdminConfig  $adminConfig
     * @return mixed
     */
    public function update(User $user, AdminConfig $adminConfig)
    {
        return $user->role->permissions()->where('title_english', 'edit-config')->get()->first() && $adminConfig ;
    }

    /**
     * Determine whether the user can delete the admin config.
     *
     * @param  \App\User  $user
     * @param  \App\AdminConfig  $adminConfig
     * @return mixed
     */
    public function delete(User $user, AdminConfig $adminConfig)
    {
        return $user->role->permissions()->where('title_english', 'delete-config')->get()->first() && $adminConfig;
    }

    /**
     * Determine whether the user can restore the admin config.
     *
     * @param  \App\User  $user
     * @param  \App\AdminConfig  $adminConfig
     * @return mixed
     */
    public function restore(User $user, AdminConfig $adminConfig)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the admin config.
     *
     * @param  \App\User  $user
     * @param  \App\AdminConfig  $adminConfig
     * @return mixed
     */
    public function forceDelete(User $user, AdminConfig $adminConfig)
    {
        //
    }
}
