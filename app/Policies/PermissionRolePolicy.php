<?php

namespace App\Policies;

use App\User;
use App\PermissionRole;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionRolePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any permission roles.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role->permissions()->where('title_english', 'read-permissionrole')->get()->first();
    }

    /**
     * Determine whether the user can view the permission role.
     *
     * @param  \App\User  $user
     * @param  \App\PermissionRole  $permissionRole
     * @return mixed
     */
    public function view(User $user, PermissionRole $permissionRole)
    {
        return $user->role->permissions()->where('title_english', 'read-permissionrole')->get()->first() && $permissionRole;
    }

    /**
     * Determine whether the user can create permission roles.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role->permissions()->where('title_english', 'add-permissionrole')->get()->first();
    }

    /**
     * Determine whether the user can update the permission role.
     *
     * @param  \App\User  $user
     * @param  \App\PermissionRole  $permissionRole
     * @return mixed
     */
    public function update(User $user, PermissionRole $permissionRole)
    {
        return $user->role->permissions()->where('title_english', 'edit-permissionrole')->get()->first() && $permissionRole;
    }

    /**
     * Determine whether the user can delete the permission role.
     *
     * @param  \App\User  $user
     * @param  \App\PermissionRole  $permissionRole
     * @return mixed
     */
    public function delete(User $user, PermissionRole $permissionRole)
    {
        return $user->role->permissions()->where('title_english', 'delete-permissionrole')->get()->first() && !empty($permissionRole) && $permissionRole->role->title != 'admin';
    }

    /**
     * Determine whether the user can restore the permission role.
     *
     * @param  \App\User  $user
     * @param  \App\PermissionRole  $permissionRole
     * @return mixed
     */
    public function restore(User $user, PermissionRole $permissionRole)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the permission role.
     *
     * @param  \App\User  $user
     * @param  \App\PermissionRole  $permissionRole
     * @return mixed
     */
    public function forceDelete(User $user, PermissionRole $permissionRole)
    {
        //
    }
}
