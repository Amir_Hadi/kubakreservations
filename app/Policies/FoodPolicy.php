<?php

namespace App\Policies;

use App\User;
use App\Food;
use Illuminate\Auth\Access\HandlesAuthorization;

class FoodPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any foods.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role->permissions()->where('title_english', 'read-food')->get()->first();
    }



    /**
     * Determine whether the user can view the food.
     *
     * @param  \App\User  $user
     * @param  \App\Food  $food
     * @return mixed
     */
    public function view(User $user, Food $food)
    {
        return $user->role->permissions()->where('title_english', 'read-food')->get()->first() && !empty($food);
    }

    /**
     * Determine whether the user can create foods.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role->permissions()->where('title_english', 'add-food')->get()->first();
    }

    /**
     * Determine whether the user can update the food.
     *
     * @param  \App\User  $user
     * @param  \App\Food  $food
     * @return mixed
     */
    public function update(User $user, Food $food)
    {
        return $user->role->permissions()->where('title_english', 'edit-food')->get()->first() && !empty($food);
    }

    /**
     * Determine whether the user can delete the food.
     *
     * @param  \App\User  $user
     * @param  \App\Food  $food
     * @return mixed
     */
    public function delete(User $user, Food $food)
    {
        return $user->role->permissions()->where('title_english', 'delete-food')->get()->first() && $food && $food->schedules->count() == 0;
    }

    /**
     * Determine whether the user can restore the food.
     *
     * @param  \App\User  $user
     * @param  \App\Food  $food
     * @return mixed
     */
    public function restore(User $user, Food $food)
    {
    }

    /**
     * Determine whether the user can permanently delete the food.
     *
     * @param  \App\User  $user
     * @param  \App\Food  $food
     * @return mixed
     */
    public function forceDelete(User $user, Food $food)
    {
        //
    }
}
