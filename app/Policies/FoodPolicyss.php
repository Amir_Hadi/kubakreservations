<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FoodPolicyss
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */

    public function create(User $user)
    {
        return $user->role->abilities('foods')->create;
    }

    public function view(User $user)
    {
        return $user->role->abilities('foods')->read;
    }

    public function update(User $user)
    {
        return $user->role->abilities('foods')->update;
    }

    public function delete(User $user)
    {
       return $user->role->abilities('foods')->delete;
    }
}
