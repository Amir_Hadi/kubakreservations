<?php

namespace App\Policies;

use App\User;
use App\Reserve;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReservePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any reserves.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role->permissions()->where('title_english', 'food-reserve')->get()->first();

    }

    /**
     * Determine whether the user can view the reserve.
     *
     * @param  \App\User  $user
     * @param  \App\Reserve  $reserve
     * @return mixed
     */
    public function view(User $user, Reserve $reserve)
    {
        return $user->role->permissions()->where('title_english', 'food-reserve')->get()->first() && $reserve;
    }

    /**
     * Determine whether the user can create reserves.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role->permissions()->where('title_english', 'food-reserve')->get()->first();


    }

    /**
     * Determine whether the user can update the reserve.
     *
     * @param  \App\User  $user
     * @param  \App\Reserve  $reserve
     * @return mixed
     */
    public function update(User $user, Reserve $reserve)
    {
        return $user->role->permissions()->where('title_english', 'food-reserve')->get()->first();


    }

    /**
     * Determine whether the user can delete the reserve.
     *
     * @param  \App\User  $user
     * @param  \App\Reserve  $reserve
     * @return mixed
     */
    public function delete(User $user, Reserve $reserve)
    {
        return $user->role->permissions()->where('title_english', 'cancel-food-reserve')->get()->first();

    }

    /**
     * Determine whether the user can restore the reserve.
     *
     * @param  \App\User  $user
     * @param  \App\Reserve  $reserve
     * @return mixed
     */
    public function restore(User $user, Reserve $reserve)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the reserve.
     *
     * @param  \App\User  $user
     * @param  \App\Reserve  $reserve
     * @return mixed
     */
    public function forceDelete(User $user, Reserve $reserve)
    {
        //
    }
}
