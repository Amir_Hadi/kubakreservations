<?php

namespace App\Policies;

use App\User;
use App\ScheduleFood;
use Illuminate\Auth\Access\HandlesAuthorization;

class ScheduleFoodPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any schedule foods.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role->permissions()->where('title_english', 'read-schedule')->get()->first();

    }

    /**
     * Determine whether the user can view the schedule food.
     *
     * @param  \App\User  $user
     * @param  \App\ScheduleFood  $scheduleFood
     * @return mixed
     */
    public function view(User $user, ScheduleFood $scheduleFood)
    {
        return $user->role->permissions()->where('title_english', 'read-schedule')->get()->first() && $scheduleFood;

    }

    /**
     * Determine whether the user can create schedule foods.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role->permissions()->where('title_english', 'add-schedule')->get()->first();
    }

    /**
     * Determine whether the user can update the schedule food.
     *
     * @param  \App\User  $user
     * @param  \App\ScheduleFood  $scheduleFood
     * @return mixed
     */
    public function update(User $user, ScheduleFood $scheduleFood)
    {
        return $user->role->permissions()->where('title_english', 'edit-schedule')->get()->first() && $scheduleFood;
    }

    /**
     * Determine whether the user can delete the schedule food.
     *
     * @param  \App\User  $user
     * @param  \App\ScheduleFood  $scheduleFood
     * @return mixed
     */
    public function delete(User $user, ScheduleFood $scheduleFood)
    {
        return $user->role->permissions()->where('title_english', 'delete-schedule')->get()->first() && $scheduleFood;
    }

    /**
     * Determine whether the user can restore the schedule food.
     *
     * @param  \App\User  $user
     * @param  \App\ScheduleFood  $scheduleFood
     * @return mixed
     */
    public function restore(User $user, ScheduleFood $scheduleFood)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the schedule food.
     *
     * @param  \App\User  $user
     * @param  \App\ScheduleFood  $scheduleFood
     * @return mixed
     */
    public function forceDelete(User $user, ScheduleFood $scheduleFood)
    {
        //
    }
}
