<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = [];

    public function users()
    {
        return $this->hasMany(\App\User::class, 'role_id');
    }

    public function accesses()
    {
        return $this->hasMany(\App\Access::class, 'role_id');
    }

    public function abilities($table)
    {
        return $this->accesses()->where('table_name', $table)->first();
    }

    public function permissions()
    {
        return $this->belongsToMany(\App\Permission::class, 'permission_roles');
    }

    public static function userRolesToProve(User $user)
    {
        return self::roleToProveRoles($user->role);
    }

    public static function roleToProveRoles(Role $role)
    {
        $roles = null;
        if($role->title == 'admin'){
            $roles = Role::all();
        }else {
            $rolesToProve = explode(', ', $role->rolesToProve);
            $rolesToProve[] = $role->id;
            $roles = Role::whereIn('id', $rolesToProve)->distinct()->get();
        }

        return $roles;
    }

}
