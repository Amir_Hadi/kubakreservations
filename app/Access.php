<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Access extends Model
{
    protected $guarded = [];

    public function role()
    {
        return $this->belongsTo(\App\Role::class, 'role_id');
    }

}
