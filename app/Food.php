<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $guarded=[];

    public function schedules()
    {
        return $this->hasMany(ScheduleFood::class, 'food_id');
    }

}
