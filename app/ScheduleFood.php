<?php

namespace App;

use App\Helper\Helper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Hekmatinasser\Verta\Verta;

class ScheduleFood extends Model
{
    protected $guarded = [];

    protected $dates = [
        'date',
    ];

    public function food()
    {
        return $this->belongsTo(Food::class, 'food_id');
    }

    public function reserves()
    {
        return $this->hasMany(Reserve::class, 'schedule_id');
    }


    public function showDateInJalali()
    {
        return Helper::toJalali($this->date);
    }

    public function dayName()
    {
        return Helper::toJalali($this->date, 'l');
    }

    public static function weeklySchedules($date=null)
    {
        if($date == null){
            $startOfWeek = Carbon::now()->startOfWeek(Carbon::SATURDAY);

            $endOfWeek = Carbon::now()->endOfWeek(Carbon::FRIDAY);
        }else{
            $startOfWeek = Carbon::parse($date)->startOfWeek(Carbon::SATURDAY);

            $endOfWeek = Carbon::parse($date)->endOfWeek(Carbon::FRIDAY);
        }



        return ScheduleFood::where('date', '>=', $startOfWeek)
                ->where('date', '<', $endOfWeek);

    }

    public static function ifHasPreviousReservations($date)
    {
        $previousWeekDate = Carbon::parse($date)->startOfWeek(Carbon::SATURDAY)->subWeek()->format('Y-m-d');

        if(ScheduleFood::weeklySchedules($previousWeekDate)->get()->count() == 0){
            return null;
        }

        return $previousWeekDate;
    }

    public static function ifNextWeekReservationAvailable($date)
    {
        $nextWeekDate = Carbon::parse($date)->addWeek()->startOfWeek(Carbon::SATURDAY)->format('Y-m-d');

        if(ScheduleFood::weeklySchedules($nextWeekDate)->get()->count() == 0){
            return null;
        }

        return $nextWeekDate;
    }

    public function schedulesReservedByUsers($date = null)
    {

    }
}
