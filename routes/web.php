<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\AdminConfig;
use Carbon\Carbon;

Route::get('/', function () {
    return view('welcome');
});



Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::delete('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/foods', 'FoodController');

Route::resource('/users', 'UserController');

Route::resource('/roles', 'RoleController');

Route::resource('/accesses', 'AccessController');


Route::get('/reserves/create', 'ReserveController@create');
//Route::post('/reserves/{schedule}/store', 'ReserveController@store');
Route::post('/reserves', 'ReserveController@store');
Route::delete('/reserves/{reserve}/destroy', 'ReserveController@destroy');

//Route::get('/schedules', 'ScheduleFoodController@index');
Route::get('/schedules/create', 'ScheduleFoodController@create');
Route::post('/schedules/store', 'ScheduleFoodController@store');
Route::get('/schedules/shownextweek', 'ScheduleFoodController@showSchedulesReservedByUsers');


Route::delete('/schedules/{scheduleFood}/reserve/destroy', 'ScheduleFoodController@deleteReservations');


Route::get('/roles/{role}/permissions', 'PermissionRoleController@create');
Route::post('/roles/{role}/permissions/store', 'PermissionRoleController@store');
Route::delete('/permissionroles/{permissionrole}/destroy', 'PermissionRoleController@destroy');

Route::get('/password/{user}/edit', 'UserController@passwordEdit');
Route::patch('/password/{user}/update', 'UserController@passwordUpdate');

Route::get('/users/{user}/editpassword', 'UserController@editOwnPassword');
Route::patch('/users/{user}/updatepassword', 'UserController@updateOwnPassword');

Route::get('/print', 'ScheduleFoodController@print');

Route::resource('/configs', 'AdminConfigController');


Route::get('/logs', 'LogController@index');


Route::delete('/schedules/{schedule}/users/{user}', 'ScheduleFoodController@deleteUserReserve');
